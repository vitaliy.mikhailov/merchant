<?php

use App\ShopCore\Bot;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Schema;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

if (Schema::connection('core')->hasTable('bots')) {
    $bots = Bot::all();

    /** @var Bot $bot */
    foreach ($bots as $bot) {
        Route::post('/' . $bot->token, 'TelegramController@index')->name('telegram');
    }
}

Route::get('/test', 'TestController@test');


Route::prefix('free-kassa')->group(function () {
    Route::get('notice', 'FreeKassaController@notice');
    Route::get('success', 'FreeKassaController@success');
    Route::get('failure', 'FreeKassaController@failure');
});

