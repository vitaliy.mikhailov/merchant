<?php

namespace App\Geo;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

/**
 * Class AlternateName
 * @package App\Geo
 *
 * @param $id
 * @param $place_id
 * @param $name
 *
 * @param Place $place
 */
class PlaceAlternateName extends Model
{
    protected $table = 'geo.place_alternate_names';

    protected $primaryKey = 'id';

    protected $fillable = ['place_id', 'language_code', 'name'];

    public function place()
    {
        return $this->belongsTo(Place::class, 'place_id');
    }

    public function lang()
    {
        return $this->belongsTo(Language::class, 'language_code');
    }
}
