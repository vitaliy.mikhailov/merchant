<?php

namespace App\Geo;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Country
 * @package App\Geo
 *
 * @property $id
 * @property $iso
 * @property $iso3
 * @property $iso_numeric
 * @property $fips
 * @property $name
 * @property $capital
 * @property $area
 * @property $population
 * @property $continent
 * @property $tld
 * @property $currency_code
 * @property $phone_prefix
 * @property $boundaries
 * @property $created_at
 * @property $updated_at
 * @property $status
 *
 * @property $getBoundaries
 */
class Country extends Model
{
    protected $table = 'geo.countries';

    protected $primaryKey = 'id';

    public function getBoundaries()
    {
        if (!empty(json_decode($this->boundaries))) {
            return self::whereIn('iso', json_decode($this->boundaries))->get();
        } else {
            return null;
        }
    }
}
