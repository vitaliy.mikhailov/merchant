<?php

namespace App\Geo;

use Illuminate\Database\Eloquent\Model;

/**
 * Class CountryAlternateName
 * @param $id
 * @param $locale
 * @param $country_id
 * @param $name
 *
 * @param Place $place
 * @package App\Geo
 *
 */
class CountryAlternateName extends Model
{

    protected $table = 'geo.country_alternate_names';

    protected $primaryKey = 'id';

    protected $fillable = ['country_id', 'name'];

    public function place()
    {
        return $this->belongsTo(Country::class, 'country_id');
    }
}
