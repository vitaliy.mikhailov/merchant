<?php

namespace App\Geo;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * Class Place
 * @package App\Geo
 *
 * @property $id
 * @property $name
 * @property $name_with_country
 * @property $ascii_name
 * @property $latitude
 * @property $longitude
 * @property $feature_code
 * @property $feature_class
 * @property $country_code
 * @property $population
 * @property $elevation
 * @property $created_at
 * @property $updated_at
 * @property $status
 *
 * @property Country $country
 *
 */
class Place extends Model
{

    protected static $countries = ['RU'];

    protected $table = 'geo.places';

    protected $primaryKey = 'id';

    protected $with = ['country'];

    public function getNameWithCountryAttribute()
    {
        return trim($this->country->name . '/' . $this->name);
    }

    public static function getPlaceByString($string)
    {
        list($country, $place) = explode('/', $string);
        /** @var Country $country */
        $country = Country::where('name', $country)->first();
        if (is_null($country)) {
            return [
                'text' => trans('errors.place_not_found')
            ];
        }
        /** @var Place $place */
        $place = Place::where(
            [
                ['name', $place],
                ['country_code', $country->iso]
            ]
        )->first();
        if (is_null($place)) {
            return [
                'text' => trans('errors.place_not_found')
            ];
        }
        return $place;
    }

    public static function getPlacesByName(string $name)
    {
        $returnPlaces = [];
        $count = 0;
        $rawQuery = "similarity(gan.\"name\", '" . $name . "'::varchar)";
        $places = DB::table('geo.alternate_names as gan')
            ->select(DB::raw('gc."name" as country_name, gp."name" as place_name'))
            ->join('geo.places as gp', 'gp.id', '=', 'gan.place_id')
            ->join('geo.countries as gc', 'gp.country_code', '=', 'gc.iso')
            ->whereRaw($rawQuery . ' > 0.5')
            ->whereIn('gp.country_code', self::$countries)
            ->orderBy(DB::raw($rawQuery))
            ->limit(100)
            ->get();
        foreach ($places as $place) {
            if (!isset($returnPlaces[$place->country_name])) {
                $returnPlaces[$place->country_name] = $place->place_name;
                $count++;
                if ($count == 5) {
                    return $returnPlaces;
                }
            }
        }
        return $returnPlaces;
    }

    public static function getNearPlace(int $latitude, int $longitude)
    {
        $rawQuery = 'population / |/((' . $latitude . ' - gp.latitude)^2 + (' . $longitude . ' - gp.longitude)^2) desc';
        $place = DB::table('geo.places as gp')
            ->where('population', '>', 30000)
            ->whereIn('gp.country_code', self::$countries)
            ->orderByRaw($rawQuery)
            ->first();
        return self::find($place->id);
    }

    public function country()
    {
        return $this->belongsTo(Country::class, 'country_code', 'iso');
    }

}
