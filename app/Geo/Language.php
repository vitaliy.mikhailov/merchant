<?php

namespace App\Geo;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Language
 * @package App\Geo
 *
 * @property $code
 * @property $name
 * @property $emoji_flag
 * @property $status
 */
class Language extends Model
{
    protected $table = 'geo.languages';

    public $incrementing = false;

    protected $primaryKey = 'code';

    protected $fillable = ['code', 'name', 'emoji_flag', 'status'];

}
