<?php

namespace App\Geo;

use Illuminate\Database\Eloquent\Model;

/**
 * Class TimeZone
 * @package App\Geo
 *
 * @property $time_zone
 * @property $country_code
 * @property $GMT
 * @property $created_at
 * @property $updated_at
 * @property $status
 *
 * @property Country $country
 *
 */
class TimeZone extends Model
{

    protected $table = 'geo.time_zones';

    protected $primaryKey = 'id';

    public function country()
    {
        return $this->belongsTo(Country::class, 'country_code', 'iso');
    }

}
