<?php

namespace App\Telegram;

use App\ShopCore\Bot;
use App\ShopCore\Shop;
use App\ShopCore\UserBotTie;
use App\Enums\StatusEnum;
use App\Enums\UserTypeEnum;
use Illuminate\Database\Eloquent\Model;

/**
 * Class TelegramUser
 * @package App/Telegram
 *
 * @property $id
 * @property $telegram_id
 * @property $language_code
 * @property $first_name
 * @property $last_name
 * @property $full_name
 * @property $username
 * @property $roles
 * @property $created_at
 * @property $updated_at
 * @property $status
 *
 * @property Shop[] $shops
 */
class TelegramUser extends Model
{

    protected $table = 'bot.telegram_users';

    protected $primaryKey = 'id';

    protected $fillable = [
        'telegram_id',
        'invite_id',
        'type',
        'language_code',
        'first_name',
        'last_name',
        'username',
        'roles',
        'status'
    ];

    public function shops()
    {
        return $this->hasMany(Shop::class, 'author_id');
    }

    public static function create(array $data, string $token)
    {
        $userEntity = new self;

        $userEntity->fill($data);
        $userEntity->save();

        /** @var Bot $bot */
        $bot = Bot::get($token);

        $tieUser = new UserBotTie();
        $tieUser->bot_id = $bot->id;
        $tieUser->user_id = $userEntity->id;
        $tieUser->role = UserTypeEnum::USER;
        $tieUser->save();

        return $userEntity;
    }

    /**
     * @param int $telegramId
     * @param string $token
     * @return TelegramUser|?
     */
    public static function get(int $telegramId, string $token)
    {
        /** @var Bot $bot */
        $bot = Bot::get($token);

        /** @var TelegramUser $user */
        $user = TelegramUser::where('telegram_id', $telegramId)->first();

        if (is_null($user)) return null;

        $tieUser = UserBotTie::where(
            [
                [
                    'user_id',
                    $user->id
                ],
                [
                    'bot_id',
                    $bot->id
                ]
            ]
        )->first();
        return !is_null($tieUser) ? $user : null;
    }

    public function getFullNameAttribute()
    {
        if (is_null($this->first_name) && is_null($this->last_name)) {
            return 'Имя не указано';
        }
        return trim($this->first_name . ' ' . $this->last_name);
    }

    /**
     * @param string $token
     * @return UserCommand|?
     */
    public function getLastCommand(string $token)
    {
        /** @var Bot $bot */
        $bot = Bot::get($token);

        $command = null;

        $command = UserCommand::where(
            [
                [
                    'user_id',
                    $this->id,
                ],
                [
                    'bot_id',
                    $bot->id
                ],
                [
                    'status',
                    StatusEnum::ENABLED
                ]
            ]
        )->orderBy('created_at', 'desc')->first();
        return $command;
    }

}
