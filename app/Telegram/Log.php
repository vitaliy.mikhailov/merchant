<?php

namespace App\Telegram;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Log
 * @package App/Telegram
 *
 * @property $id
 * @property $user_id
 * @property $request
 * @property $response_code
 * @property $response
 * @property $created_at
 * @property $updated_at
 * @property $status
 */
class Log extends Model
{
    protected $table = 'system.logs';

    protected $primaryKey = 'id';

    protected $fillable = ['user_id', 'request', 'response_code', 'response', 'status'];

    public function user()
    {
        return $this->belongsTo(TelegramUser::class, 'user_id');
    }

    public static function create($data)
    {
        $log = new self;
        $log->fill($data);
        $log->save();
        return $log;
    }

    public function setResponseAttribute()
    {
        $this->response = json_encode($this->response);
    }
}
