<?php

namespace App\Telegram;

use App\Enums\StatusEnum;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

/**
 * Class Button
 * @package Telegram
 *
 * @property $id
 * @property $command_id
 * @property $language_code
 * @property $text
 * @property $status
 */
class ButtonLocalization extends Model
{

    protected $table = 'system.button_localizations';

    protected $primaryKey = 'id';

    protected $fillable = ['language_code', 'command', 'text', 'status'];

    public $timestamps = false;

}
