<?php

namespace App\Telegram;

use App\Enums\StatusEnum;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

/**
 * Class Button
 * @package Telegram
 *
 * @property $id
 * @property $section
 * @property $command
 * @property $text
 * @property $status
 *
 * @property ButtonLocalization $localizations
 */
class Button extends Model
{
    protected $table = 'system.buttons';

    protected $primaryKey = 'id';

    protected $fillable = ['section', 'command', 'status'];

    public $timestamps = false;

    public function getTextAttribute()
    {
        /** @var ButtonLocalization $localization */
        $localization = $this->localizations->where('language_code', App::getLocale())->first();
        return $localization->text;
    }

    public function localizations()
    {
        return $this->hasMany(ButtonLocalization::class, 'command_id');
    }

    public static function getButtonList()
    {
        $locale = App::getLocale();

        $where = [
            [
                'status',
                StatusEnum::ENABLED
            ]
        ];

        $buttons = self::where($where)->orWhere('command', 'setLanguage')->orderBy('id')->get();

        $buttonList = [];

        /** @var self $button */
        foreach ($buttons as $button) {

            if (!is_null($locale)) {
                $where[] = [
                    'language_code',
                    $locale
                ];
            }

            /** @var ButtonLocalization $localization */
            foreach ($button->localizations as $localization) {
                $buttonList[] = [
                    'section' => $button->section,
                    'command' => $button->command,
                    'text' => $localization->text,
                    'language_code' => $localization->language_code
                ];
            }
        }

        return $buttonList;
    }
}
