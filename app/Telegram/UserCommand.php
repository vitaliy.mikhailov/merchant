<?php

namespace App\Telegram;

use App\Enums\StatusEnum;
use App\ShopCore\Bot;
use Illuminate\Database\Eloquent\Model;

/**
 * Class UserCommand
 * @package App/Telegram
 *
 * @property $id
 * @property $user_id
 * @property $bot_id
 * @property $type
 * @property $data
 * @property $created_at
 * @property $updated_at
 * @property $status
 *
 * @property TelegramUser $user
 */
class UserCommand extends Model
{
    protected $table = 'bot.user_commands';

    protected $primaryKey = 'id';

    protected $fillable = ['user_id', 'bot_id', 'type', 'data', 'status'];

    protected $casts = [
        'data' => 'array'
    ];

    public function user()
    {
        return $this->belongsTo(TelegramUser::class, 'user_id');
    }

    public static function create(TelegramUser $user, string $token, string $type, array $data = null)
    {
        $command = $user->getLastCommand($token);

        if (!is_null($command) && $command->type == $type) {
            $command->data = $data;
            $command->status = StatusEnum::ENABLED;
            $command->save();
        } else {
            self::rejectAllCommands($user, $token);

            $bot = Bot::get($token);

            $commandData = [
                'user_id' => $user->id,
                'bot_id' => $bot->id,
                'type' => $type,
                'data' => $data
            ];
            $command = new UserCommand;
            $command->fill($commandData);
            $command->save();
        }
        return true;
    }

    public static function rejectAllCommands(TelegramUser $user, string $token)
    {
        $bot = Bot::get($token);

        self::where([['user_id', $user->id], ['bot_id', $bot->id], ['status', StatusEnum::ENABLED]])
            ->update(['status' => StatusEnum::REJECTED]);
    }
}
