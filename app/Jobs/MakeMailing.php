<?php

namespace App\Jobs;

use App\Enums\TelegramEnum;
use App\Helpers\TelegramHelper;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;

class MakeMailing implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $userIds;
    protected $responseParams;

    public function __construct($userIds, $responseParams)
    {
        $this->userIds = $userIds;
        $this->responseParams = $responseParams;
    }

    public function handle()
    {
        $responseParams = $this->responseParams;
        foreach ($this->userIds as $id) {
            $responseParams['chat_id'] = $id;
            TelegramHelper::responseTelegram(TelegramEnum::SEND_MESSAGE, $responseParams);
        }
    }
}
