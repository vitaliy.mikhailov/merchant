<?php

namespace App\Jobs;

use App\Geo\Country;
use App\Geo\CountryAlternateName;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

class TranslatePlaces implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $language;

    public function __construct($language)
    {
        $this->language = $language;
    }

    private static $basePath = 'https://translate.yandex.net/api/v1.5/tr.json/translate';

    public function handle()
    {
        $namesCount = CountryAlternateName::where('language_code', $this->language)->count();
        $placeCount = Country::all()->count();

        if ($namesCount < $placeCount) {
            $places = Country::orderBy('id')->limit(20)->offset($namesCount)->get();

            $params = [
                'key' => config('app.yandex_translate_api_key'),
                'lang' => 'en-' . $this->language,
            ];

            $url = self::$basePath . '?' . http_build_query($params);

            /** @var Country $place */
            foreach ($places as $place) {
                $url .= '&text=' . urlencode($place->name);
            }

            $url = ltrim($url, '&');

            $data = json_decode(file_get_contents($url), true);

            $insertData = [];

            if ($data['code'] == 200) {
                foreach ($data['text'] as $key => $name) {
                    $insertData[] = [
                        'country_id' => $places[$key]->id,
                        'language_code' => $this->language,
                        'name' => $name,
                        'created_at' => date('Y-m-d H:i:s', time() - (6 * 60 * 60)),
                        'updated_at' => date('Y-m-d H:i:s', time() - (6 * 60 * 60)),
                    ];
                }

                DB::table('geo.country_alternate_names')->insert($insertData);

                self::dispatch($this->language);
            }
        }
    }
}
