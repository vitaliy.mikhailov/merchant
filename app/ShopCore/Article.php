<?php

namespace App\ShopCore;

use App\Enums\StatusEnum;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Article
 * @package App/ShopCore
 *
 * @property $id
 * @property $title
 * @property $short_content
 * @property $link
 * @property $publication_date
 * @property $created_at
 * @property $updated_at
 * @property $status
 */
class Article extends Model
{
    public const BASE_PATH = 'https://telegra.ph/';

    protected $table = 'core.articles';

    protected $primaryKey = 'id';

    protected $fillable = ['title', 'short_content', 'link', 'publication_date', 'status'];

    public static function getArticles($page = 1)
    {
        $page -= 1;
        $limit = 5;
        $totalPage = intval(ceil(self::all()->count() / $limit));

        return [
            'current' => self::where('status', StatusEnum::ENABLED)
                ->orderBy('created_at', 'desc')
                ->limit($limit)
                ->offset($limit * $page)
                ->get(),
            'page' => $page + 1,
            'total' => $totalPage
        ];
    }
}
