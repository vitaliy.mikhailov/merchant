<?php

namespace App\ShopCore;

use App\Geo\Place;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Bot
 * @package App\ShopCore
 *
 * @property $id
 * @property $shop_id
 * @property $place_shop_tie_id
 * @property $name
 * @property $created_at
 * @property $updated_at
 * @property $status
 */
class ShopCategories extends Model
{
    protected $table = 'core.shop_categories';

    protected $primaryKey = 'id';

    protected $fillable = ['shop_id', 'place_shop_tie_id', 'name', 'status'];

    public function shop()
    {
        return $this->belongsTo(Shop::class, 'shop_id');
    }

    public function place()
    {
        // TODO: check it
        return $this->hasOneThrough(Place::class, PlaceShopTie::class, 'id', 'id', 'place_shop_tie_id', 'place_id');
    }
}
