<?php

namespace App\ShopCore;

use App\Geo\Place;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Bot
 * @package App\ShopCore
 *
 * @property $id
 * @property $shop_id
 * @property $place_id
 * @property $created_at
 * @property $updated_at
 * @property $status
 */
class PlaceShopTie extends Model
{
    protected $table = 'core.place_shop_ties';

    protected $primaryKey = 'id';

    protected $fillable = ['shop_id', 'place_id', 'status'];

    public function shop()
    {
        return $this->belongsTo(Shop::class,'shop_id');
    }

    public function place()
    {
        return $this->belongsTo(Place::class,'place_id');
    }
}
