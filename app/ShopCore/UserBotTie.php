<?php

namespace App\ShopCore;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Bot
 * @package App\ShopCore
 *
 * @property $id
 * @property $user_id
 * @property $bot_id
 * @property $role
 * @property $rules
 * @property $created_at
 * @property $updated_at
 * @property $status
 */
class UserBotTie extends Model
{
    protected $table = 'core.user_bot_ties';

    protected $primaryKey = 'id';
}
