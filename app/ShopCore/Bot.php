<?php

namespace App\ShopCore;

use App\Telegram\TelegramUser;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Bot
 * @package App\ShopCore
 *
 * @property $id
 * @property $type
 * @property $shop_id
 * @property $telegram_id
 * @property $first_name
 * @property $username
 * @property $token
 * @property $created_at
 * @property $updated_at
 * @property $status
 *
 * @property TelegramUser $author
 * @property Shop $shop
 */
class Bot extends Model
{
    protected $table = 'core.bots';

    protected $primaryKey = 'id';

    protected $fillable = ['type', 'shop_id', 'telegram_id', 'first_name', 'username', 'token'];

    public static function get(string $token): ?Bot
    {
        return Bot::where('token', $token)->first();
    }

    public function shop()
    {
        return $this->belongsTo(Shop::class, 'shop_id');
    }

    public function author()
    {
        return $this->hasOneThrough(TelegramUser::class, Shop::class, 'id', 'id', 'user_id', 'author_id');
    }

    public static function create($data)
    {
        $bot = new self;
        $bot->fill($data);
        $bot->save();
        return $bot;
    }
}
