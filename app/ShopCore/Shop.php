<?php

namespace App\ShopCore;

use App\Enums\BotTypeEnum;
use App\Enums\ShopStatusEnum;
use App\Enums\UserTypeEnum;
use App\Geo\Place;
use App\Telegram\TelegramUser;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Bot
 * @package App\ShopCore
 *
 * @property $id
 * @property $author_id
 * @property $name
 * @property $customer_count
 * @property $frozen_amount
 * @property $payout_amount
 * @property $place_count
 * @property $category_count
 * @property $personal_count
 * @property $rate
 * @property $created_at
 * @property $updated_at
 * @property $status
 *
 * @property TelegramUser $author
 *
 * @property Bot $shop
 * @property Bot $chat
 *
 * @property Place[] $places
 * @property ShopCategories[] $categories
 */
class Shop extends Model
{
    protected $table = 'core.shops';

    protected $primaryKey = 'id';

    protected $fillable = ['author_id', 'name', 'status'];

    public function author()
    {
        return $this->belongsTo(TelegramUser::class, 'author_id');
    }

    public function shop()
    {
        return $this->hasOne(Bot::class, 'shop_id')->where('type', BotTypeEnum::SHOP);
    }

    public function chat()
    {
        return $this->hasOne(Bot::class, 'shop_id')->where('type', BotTypeEnum::CHAT);
    }

    public function places()
    {
        return $this->hasManyThrough(Place::class, PlaceShopTie::class, 'id', 'id', 'place_id', 'shop_id');
    }

    public function categories()
    {
        return $this->hasMany(ShopCategories::class, 'shop_id');
    }

    public function getCustomerCountAttribute()
    {
        return UserBotTie::where('bot_id', $this->id)->where('role', UserTypeEnum::USER)->count();
    }

    public function getFrozenAmountAttribute()
    {
        // TODO: Make it
        $frozenAmount = 0;
        return number_format(round($frozenAmount, 2), 2);
    }

    public function getPayoutAmountAttribute()
    {
        // TODO: Make it
        $payoutAmount = 0;
        return number_format(round($payoutAmount, 2), 2);
    }

    public function getRateAttribute()
    {
        // TODO: Make it
        $rate = 5;
        return round($rate, 1);
    }

    public function getPlaceCountAttribute()
    {
        return PlaceShopTie::where('shop_id', $this->id)->count();
    }

    public function getPersonalCountAttribute()
    {
        return UserBotTie::where('user_id', '!=', $this->author_id)->whereIn('role', [UserTypeEnum::MANAGER])->count();
    }

    public function getShopAttribute()
    {
        return ShopCategories::where('shop_id', $this->id)->count();
    }

    public function getOneShopText()
    {
        return trans('messages.shop_base') . PHP_EOL .
            trans('messages.shop_name', ['name' => $this->name]) . PHP_EOL .
            trans('messages.shop_customer_count', ['customerCount' => $this->customer_count]) . PHP_EOL . PHP_EOL .
            trans('messages.shop_wallet') . PHP_EOL .
            trans('messages.shop_frozen_amount', ['frozenAmount' => $this->frozen_amount]) . PHP_EOL .
            trans('messages.shop_payout_amount', ['payoutAmount' => $this->payout_amount]) . PHP_EOL . PHP_EOL .
            trans('messages.shop_data') . PHP_EOL .
            trans('messages.shop_rate', ['rate' => $this->rate]) . PHP_EOL .
            trans_choice('messages.shop_status', $this->status);
    }

    public function getPlaceText($page = 0)
    {
        $places = $this->places->limit(10)->offset($page);

        return trans('messages.shop_base') . PHP_EOL .
            trans('messages.shop_name', ['name' => $this->name]) . PHP_EOL .
            trans('messages.shop_customer_count', ['customerCount' => $this->customer_count]) . PHP_EOL . PHP_EOL .
            trans('messages.shop_wallet') . PHP_EOL .
            trans('messages.shop_frozen_amount', ['frozenAmount' => $this->frozen_amount]) . PHP_EOL .
            trans('messages.shop_payout_amount', ['payoutAmount' => $this->payout_amount]) . PHP_EOL . PHP_EOL .
            trans('messages.shop_data') . PHP_EOL .
            trans('messages.shop_rate', ['rate' => $this->rate]) . PHP_EOL .
            trans_choice('messages.shop_status', $this->status);
    }

    public static function enableAllShop(TelegramUser $user)
    {
        /** @var Shop[] $shops */
        $shops = Shop::where('author_id', $user->id)->get();

        $shopIds = [];

        foreach ($shops as $shop) {
            if ($shop->places->count() != 0 && $shop->categories->count() != 0) {
                $shopIds[] = $shop->id;
            }
        }

        self::whereIn('id', $shopIds)->update(['status' => ShopStatusEnum::ENABLED]);
    }

    public static function create($data)
    {
        $shop = new self;
        $shop->fill($data);
        $shop->save();
        return $shop;
    }
}
