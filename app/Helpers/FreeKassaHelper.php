<?php

namespace App\Helpers;

/**
 * Class FreeKassaHelper
 * @package App\Helpers
 *
 * @method getBalance(): float
 * @method checkOrderInfo(string $orderId): array
 * @method payout(string $wallet, float $amount): array
 * @method createBill(string $orderId, float $amount, string $email): array
 *
 * @method cashOut(string $wallet, float $amount, int $currency, string $description, int $disable_exchange) array
 * @method getCashOutStatus(integer $paymentId) string
 * @method cashTransfer(string $wallet, float $amount) string
 */
class FreeKassaHelper
{
    private $url;

    private $walletId;
    private $APIKey;

    private $merchantId;
    private $secret;

    public $type;

    public const HTTP_GET = 'GET';
    public const HTTP_POST = 'POST';

    public const MERCHANT = 'merchant';
    public const WALLET = 'wallet';

    public const WALLET_URL = 'https://www.fkwallet.ru/api_v1.php';

    public const MERCHANT_URL = 'https://www.free-kassa.ru/api.php';

    public const METHODS = [
        self::MERCHANT => [
            'getBalance' => [
                'method' => self::HTTP_GET,
                'action' => 'get_balance',
                'sign' => ['merchantId', 'secret'],
                'requiredFields' => ['s' => 'sign', 'merchant_id' => 'merchantId'],
                'notRequiredFields' => [],
                'validate' => [],
                'return' => ['path' => 'data/balance', 'type' => 'int']
            ],
            'checkOrderInfo' => [
                'method' => self::HTTP_GET,
                'action' => 'check_order_status',
                'sign' => ['merchantId', 'secret'],
                'requiredFields' => ['s' => 'sign', 'merchant_id' => 'merchantId', 'order_id' => 'orderId'],
                'notRequiredFields' => [],
                'validate' => [],
                'return' => ['path' => 'data/balance', 'type' => 'int']
            ],
            'payout' => [
                'method' => self::HTTP_GET,
                'action' => 'payment',
                'sign' => ['merchantId', 'secret'],
                'requiredFields' => [
                    's' => 'sign',
                    'merchant_id' => 'merchantId',
                    'currency' => 'wallet',
                    'amount' => 'amount'
                ],
                'notRequiredFields' => [],
                'validate' => ['amount' => ['min' => 50, 'max' => 150000], 'wallet' => ['in' => ['fkw']]],
                'return' => ['path' => 'data/balance', 'type' => 'int']
            ],
            'createBill' => [
                'method' => self::HTTP_GET,
                'action' => 'create_bill',
                'sign' => ['merchantId', 'secret'],
                'requiredFields' => [
                    's' => 'sign',
                    'merchant_id' => 'merchantId',
                    'desc' => 'orderId',
                    'amount' => 'amount',
                    'email' => 'email'
                ],
                'notRequiredFields' => [],
                'validate' => [],
                'return' => ['path' => 'desc']
            ],
        ],
        self::WALLET => [
            'getBalance' => [
                'method' => self::HTTP_POST,
                'action' => 'get_balance',
                'sign' => ['walletId', 'APIKey'],
                'requiredFields' => ['sign' => 'sign', 'wallet_id' => 'walletId'],
                'notRequiredFields' => [],
                'validate' => [],
                'return' => ['path' => 'data']
            ],
            'cashOut' => [
                'method' => self::HTTP_POST,
                'action' => 'cashout',
                'sign' => ['walletId', 'currency', 'amount', 'wallet', 'APIKey'],
                'requiredFields' => [
                    'sign' => 'sign',
                    'wallet_id' => 'walletId',
                    'purse' => 'wallet',
                    'amount' => 'amount',
                    'currency' => 'currency',
                    'desc' => 'description',
                    'disable_exchange' => 'disable_exchange',
                ],
                'notRequiredFields' => [],
                'validate' => [],
                'return' => ['path' => 'data']
            ],
            'cashTransfer' => [
                'method' => self::HTTP_POST,
                'action' => 'transfer',
                'sign' => ['walletId', 'wallet', 'amount', 'APIKey'],
                'requiredFields' => [
                    'sign' => 'sign',
                    'wallet_id' => 'walletId',
                    'purse' => 'wallet',
                    'amount' => 'amount',
                ],
                'notRequiredFields' => [],
                'validate' => [],
                'return' => ['path' => 'data']
            ],
        ]
    ];

    public function __construct($type, $accountId = null, $secret = null)
    {
        if ($type == self::MERCHANT) {
            $this->merchantId = is_null($accountId) ? config('app.free_kassa_merchant_id') : $accountId;
            $this->secret = is_null($secret) ? config('app.free_kassa_merchant_secret') : $secret;
            $this->url = self::MERCHANT_URL;
        } elseif ($type == self::WALLET) {
            $this->walletId = is_null($accountId) ? config('app.free_kassa_wallet_id') : $accountId;
            $this->APIKey = is_null($secret) ? config('app.free_kassa_wallet_api_key') : $secret;
            $this->url = self::WALLET_URL;
        }
        $this->type = $type;
    }

    public function createBillUrl(
        string $orderId,
        float $amount,
        int $currencyId = null,
        string $email = null,
        string $lang = 'ru'
    ) {
        if ($this->type != self::MERCHANT) {
            return [
                'error' => 1,
                'message' => 'Эта функция доступна только в режиме магазина'
            ];
        }

        $url = 'https://www.free-kassa.ru/merchant/cash.php';

        $signString = $this->merchantId . ':' . $amount . ':' . $this->secret . ':' . $orderId;

        $params = [
            'm' => $this->merchantId,
            'oa' => $amount,
            'o' => $orderId,
            's' => md5($signString),
            'lang' => $lang,
            'i' => $currencyId,
            'em' => $email
        ];

        return $url . '?' . http_build_query($params);
    }

    public function __call($name, $arguments)
    {
        if (!in_array($this->type, [self::WALLET, self::MERCHANT])) {
            return [
                'error' => 1,
                'message' => 'Укажите верный тип: кошелек или магазин'
            ];
        }

        if (!isset(self::METHODS[$this->type][$name])) {
            return [
                'error' => 1,
                'message' => 'Нет такого метода'
            ];
        }

        $method = self::METHODS[$this->type][$name];

        $requiredFields = $method['requiredFields'];
        $notRequiredFields = $method['notRequiredFields'];

        $payload = [
            'action' => $method['action'],
            'type' => 'json'
        ];

        foreach ($requiredFields as $merchantField => $functionParam) {
            if (property_exists($this, $functionParam)) {
                $payload[$merchantField] = $this->$functionParam;
                unset($requiredFields[$merchantField]);
            }
        }

        if (in_array('sign', $requiredFields)) {
            $signString = '';
            foreach ($method['sign'] as $param) {
                if (property_exists($this, $param)) {
                    $signString .= $this->$param;
                } else {
                    $signString .= $arguments[$param];
                }
            }
            $sign = md5($signString);
            array_unshift($arguments, $sign);
        }

        $requiredArguments = array_slice($arguments, 0, count($requiredFields));
        $notRequiredArguments = array_slice($arguments, count($requiredFields) - 1);

        $arguments = array_combine($requiredFields, $requiredArguments);

        if (!empty($notRequiredFields)) {
            $arguments += array_combine($notRequiredFields, $notRequiredArguments);
        }

        $maxFieldsCount = count($requiredFields) + count($notRequiredFields);
        $currentFields = count($arguments);

        if ($maxFieldsCount < $currentFields) {
            return [
                'error' => 1,
                'message' => 'В функцию переданно слишком много параметров. Ожидается: ' . $maxFieldsCount . '. Переданно: ' . $currentFields . '.'
            ];
        }

        $check = $this->validate($arguments, $method['validate']);

        if ($check['error']) {
            return $check;
        }

        foreach ($requiredFields as $merchantField => $functionParam) {
            if (isset($arguments[$functionParam])) {
                $payload[$merchantField] = $arguments[$functionParam];
            } else {
                return [
                    'error' => 1,
                    'message' => 'Поле ' . $functionParam . ' является обязательным'
                ];
            }
        }

        foreach ($notRequiredFields as $merchantField => $functionParam) {
            if (isset($arguments[$functionParam])) {
                $payload[$merchantField] = $arguments[$functionParam];
            }
        }

        $options = [
            CURLOPT_URL => $this->url,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_RETURNTRANSFER => true
        ];

        if ($method['method'] == self::HTTP_GET) {
            $options[CURLOPT_URL] .= '?' . http_build_query($payload);
        } elseif ($method['method'] == self::HTTP_POST) {
            $options[CURLOPT_POST] = true;
            $options[CURLOPT_POSTFIELDS] = $payload;
        }

        $curl = curl_init();
        curl_setopt_array($curl, $options);

        $returnTransfer = json_decode(curl_exec($curl), true);

        $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        if ($statusCode != 200) {
            return [
                'error' => 1,
                'status_code' => $statusCode,
                'error_code' => curl_error($curl),
                'response' => $returnTransfer
            ];
        } else {
            $path = explode('/', $method['return']['path']);
            foreach ($path as $point) {
                $returnTransfer = $returnTransfer[$point];
            }
            return [
                'error' => 0,
                'response' => $returnTransfer
            ];
        }
    }

    public function validate($payload, $validate)
    {
        $return = [
            'error' => 0,
            'messages' => []
        ];

        foreach ($payload as $key => $value) {
            if (isset($validate[$key])) {
                foreach ($validate[$key] as $rule => $param) {
                    switch ($rule) {
                        case 'min':
                            if ($value < $param) {
                                $return['error'] = 1;
                                $return['messages'][] = 'Аргумемнт ' . $key . ' должен быть больше ' . $param;
                            }
                            break;

                        case 'max':
                            if ($value > $param) {
                                $return['error'] = 1;
                                $return['messages'][] = 'Аргумемнт ' . $key . ' должен быть меньше ' . $param;
                            }
                            break;

                        case 'in':
                            if (!in_array($value, $param)) {
                                $return['error'] = 1;
                                $return['messages'][] = 'Аргумемнт ' . $key . ' может принимать значения: "' . implode(
                                        '", "',
                                        $param
                                    ) . '"';
                            }
                            break;
                    }
                }
            }
        }

        return $return;
    }

}
