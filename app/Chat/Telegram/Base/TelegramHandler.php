<?php

namespace App\Chat\Telegram\Base;

use App\Enums\TelegramEnum;
use App\Jobs\SendTelegramRequest;
use App\ShopCore\Bot;
use App\Telegram\Button;
use App\Telegram\Log;
use App\Telegram\TelegramUser;
use Illuminate\Support\Facades\App;

class TelegramHandler extends BaseHandler
{

    private const BASE_PATH = 'App\\Chat\\Telegram';

    private const MESSAGE = 'MessageHandler';

    private const COMMAND = 'CommandHandler';

    private const CALLBACK = 'CallbackQueryHandler';


    public static $basePath = 'https://api.telegram.org/';

    public function __construct($data, $token)
    {
        $this->data = $data;
        $this->token = $token;

        if (isset($this->data['message']['from']['id'])) {
            $this->telegramId = $data['message']['from']['id'];
        } elseif (isset($data['callback_query']['from']['id'])) {
            $this->telegramId = $data['callback_query']['from']['id'];
        }

        $logData = [
            'request' => json_encode($data)
        ];

        $this->log = Log::create($logData);
    }

    public function parseTelegramRequest()
    {
        if (!$this->telegramId) {
            $this->log->response_code = 200;
            $this->log->response = 'ok';
            $this->log->save();
            return response('ok');
        }

        $this->log->user_id = $this->telegramId;
        $this->log->save();

        if (config('app.response') != true) {
            $this->log->response_code = 200;
            $this->log->response = 'ok';
            $this->log->save();
            return response('ok');
        }

        /** @var TelegramUser $user */
        $user = TelegramUser::get($this->telegramId, $this->token);
        $locale = $user ? $user->language_code : 'ru';
        App::setLocale($locale);

        $username = (isset($this->data['message']['from']['username']) && !is_null(
                $this->data['message']['from']['username']
            )) ? $this->data['message']['from']['username'] : '';
        if ($user && !empty($username) && $user->username != $username) {
            $user->username = $username;
            $user->save();
        }


        if (isset($this->data['message']['text'])) {
            $command = $this->getCommand();
        } else {
            $command = [
                'section' => 'Main',
                'command' => 'unknown'
            ];
        }

        $lastCommand = !is_null($user) ? $user->getLastCommand($this->token) : null;

        $lastCommand = !is_null($lastCommand) ? $lastCommand->type : null;

        /*if (isset($this->data['message']['contact']) && $lastCommand == CommandEnum::SET_PERSONAL_PHONE) {
            // Обработка контакта
            $responseParams = CommandHelper::setPersonalPhone($this->data);
        } else*/

        $messageHandler = self::getHandler(self::MESSAGE, $command['section']);

        if (!is_null($user) && !is_null(
                $lastCommand
            ) && !isset($this->data['callback_query']) && $command['command'] == 'unknown') {
            // Обработка команд
            list($section, $method) = explode('.', $lastCommand);
            $commandHandler = self::getHandler(self::COMMAND, $section);
            $commandHandler = new $commandHandler($this->data, $this->token);
            if (method_exists($commandHandler, $method)) {
                $responseParams = $commandHandler->$method();
            } else {
                $responseParams = $messageHandler->unknown();
            }
        } elseif (isset($this->data['callback_query'])) {
            //  Обработка inline клавиатуры
            $this->responseTelegram(
                TelegramEnum::ANSWER_CALLBACK_QUERY,
                ['callback_query_id' => $this->data['callback_query']['id']]
            );
            $data = json_decode($this->data['callback_query']['data'], true);
            $command = explode('.', $data['command']);
            $callbackCommandSection = $command[0];
            $callbackQueryHandler = self::getHandler(self::CALLBACK, $callbackCommandSection);
            $callbackQueryHandler = new $callbackQueryHandler($this->data, $this->token);
            $callbackCommand = $command[1];
            if (method_exists($callbackQueryHandler, $callbackCommand)) {
                $callbackQueryHandler->$callbackCommand();
            }
            return response('ok');
        }

        if (!isset($responseParams) && !method_exists($messageHandler, $command['command'])) {
            $responseParams = $messageHandler->unknown();
        }

        $command = $command['command'];

        if (!isset($responseParams)) {
            $messageHandler = new $messageHandler($this->data, $this->token);
            $responseParams = $responseParams ?? $messageHandler->$command();
        }

        if (isset($responseParams['status']) && $responseParams['status'] == 'ok') {
            $this->log->response_code = 200;
            $this->log->response = 'ok';
            $this->log->save();
            return response('ok');
        }

        $responseParams['chat_id'] = strval($this->data['message']['chat']['id']);
        $responseParams['parse_mode'] = $responseParams['parse_mode'] ?? 'Markdown';

        $this->log->response = json_encode($responseParams);
        $this->log->save();

        return $this->responseTelegram(TelegramEnum::SEND_MESSAGE, $responseParams);
    }

    public function responseTelegram(string $action, array $requestParams)
    {
        $url = self::$basePath . "bot" . $this->token . '/' . $action . '?' . http_build_query(
                $requestParams
            );

        if (config('app.testing')) {
            $options = [
                CURLOPT_SSL_VERIFYHOST => false,
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true
            ];

            $curl = curl_init();
            curl_setopt_array($curl, $options);

            $returnTransfer = curl_exec($curl);

            $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

            $this->log->response_code = $statusCode;
            $this->log->save();

            if ($statusCode != 200) {
                return [
                    'status_code' => $statusCode,
                    'response' => json_decode($returnTransfer, true),
                    'error_code' => curl_error($curl)
                ];
            } else {
                return json_decode($returnTransfer, true);
            }
        } else {
            SendTelegramRequest::dispatch($url)->onQueue('High');
            return true;
        }
    }

    public static function requestTelegram(string $token, string $action, array $requestParams = [])
    {
        $url = self::$basePath . 'bot' . $token . '/' . $action . (!empty($requestParams) ? '?' . http_build_query(
                    $requestParams
                ) : null);

        if (config('app.testing')) {
            $options = [
                CURLOPT_SSL_VERIFYHOST => false,
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true
            ];

            $curl = curl_init();
            curl_setopt_array($curl, $options);

            $returnTransfer = curl_exec($curl);

            $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

            if ($statusCode != 200) {
                return [
                    'status_code' => $statusCode,
                    'response' => json_decode($returnTransfer, true),
                    'error_code' => curl_error($curl)
                ];
            } else {
                return json_decode($returnTransfer, true);
            }
        } else {
            SendTelegramRequest::dispatch($url)->onQueue('High');
            return true;
        }
    }

    private function getCommand()
    {
        $message = $this->data['message']['text'];
        $command = [
            'section' => 'Main',
            'command' => 'unknown'
        ];
        if (strpos($message, '/start') !== false) {
            $message = '/start';
        }

        $buttonArray = Button::getButtonList();

        if (strlen($message) > 5) {
            foreach ($buttonArray as $button) {
                if (strpos($button['text'], $message) !== false) {
                    $command = [
                        'section' => $button['section'],
                        'command' => $button['command']
                    ];
                    if ($command['section'] == 'main' && $command['command'] == 'setLanguage') {
                        App::setLocale($button['language_code']);
                    }
                    break;
                }
            }
        }
        return $command;
    }

    private function getHandler(string $type, string $section)
    {
        $bot = Bot::get($this->token);

        $botType = '';

        if (is_null($bot->author)) {
            $botType .= 'Core';
        }

        $botType .= ucfirst($bot->type);

        return self::BASE_PATH . '\\' . $botType . '\\' . ucfirst($section) . '\\' . $type;
    }
}
