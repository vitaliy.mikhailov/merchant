<?php

namespace App\Chat\Telegram\Base;

use App\Telegram\TelegramUser;

class BaseHandler
{

    public $request;
    public $token;
    public $message;

    /** @var TelegramUser $user */
    public $user;
    public $data;
    public $log;
    public $telegramId;

    public function __call($method, $args)
    {
        return call_user_func_array(
            $method,
            $args
        );
    }

    public static function noData($request)
    {
        return response('ok');
    }

}
