<?php

namespace App\Chat\Telegram\Base;

use App\Telegram\TelegramUser;

class BaseCallbackQueryHandler extends BaseHandler
{

    public function __construct($data, $token)
    {
        $this->data = $data;
        $this->token = $token;
        $this->user = TelegramUser::get($data['callback_query']['from']['id'], $token);
    }



}
