<?php

namespace App\Chat\Telegram\Base;

use App\Telegram\TelegramUser;

class BaseCommandHandler extends BaseHandler
{

    public const PHONE_VALIDATE = '/^((\+?7|8)(?!95[4-79]|99[08]|907|94[^0]|336)([348]\d|9[0-6789]|7[0247])\d{8}|\+?(99[^4568]\d{7,11}|994\d{9}|9955\d{8}|996[57]\d{8}|9989\d{8}|380[34569]\d{8}|375[234]\d{8}|372\d{7,8}|37[0-4]\d{8}))$/';

    public function __construct(array $request, string $token)
    {
        $this->request = $request;
        $this->token = $token;

        $this->user = TelegramUser::get($request['message']['from']['id'], $token);
    }

}
