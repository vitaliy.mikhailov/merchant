<?php

namespace App\Chat\Telegram\Base;

class BaseKeyboard
{

    public static function back()
    {
        return json_encode(
            [
                'keyboard' =>
                    [
                        [
                            [
                                'text' => trans('buttons.main.back')
                            ]
                        ]
                    ],
                'one_time_keyboard' => false,
                'resize_keyboard' => true
            ]
        );
    }

    public static function supportButton(string $url)
    {
        return json_encode(
            [
                'inline_keyboard' => [
                    [
                        [
                            'text' => trans('buttons.main.support'),
                            'url' => $url
                        ],
                    ]
                ]
            ]
        );
    }

}
