<?php

namespace App\Chat\Telegram\Base;

use App\Enums\CommandEnum;
use App\Telegram\TelegramUser;

class BaseMessageHandler extends BaseHandler
{

    public function __construct(array $request, string $token, string $message = null, array $params = [])
    {
        $this->request = $request;
        $this->token = $token;

        $this->message = !is_null($message) ? (is_null($params) ? trans($message) : trans($message, $params)) : null;

        $this->user = TelegramUser::get($request['message']['from']['id'], $token);
    }

    public function back(): array
    {
        $command = $this->user->getLastCommand($this->token);

        $returnMenu = [
            'mainMenu' => [
                CommandEnum::DISCLAIMER,
                CommandEnum::PRICE_POLICE,
                CommandEnum::GET_ONE_SHOP,
                CommandEnum::ARTICLES,
            ],
            'shops' => [
                CommandEnum::SET_SHOP_TOKENS,
            ]
        ];

        foreach ($returnMenu as $function => $current) {
            if (in_array($command->type, $current)) return $this->$function();
        }

        return [
            'text' => trans('messages.unknown')
        ];
    }

    public function unknown(): array
    {
        $command = $this->user->getLastCommand($this->token);
        if (isset($command->type)) {
            $command = $command->type;
            return (new BaseCommandHandler)->$command($this->request);
        } else {
            return [
                'text' => trans('messages.unknown')
            ];
        }
    }

}
