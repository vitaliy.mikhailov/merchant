<?php

namespace App\Chat\Telegram\CoreShop\Place;

use App\Chat\Telegram\Base\BaseMessageHandler;
use App\Chat\Telegram\Base\TelegramHandler;
use App\Chat\Telegram\CoreShop\Main\MessageHandler as MainMessageHandler;
use App\Enums\BotTypeEnum;
use App\Enums\CommandEnum;
use App\Enums\ShopStatusEnum;
use App\Enums\TelegramEnum;
use App\ShopCore\Bot;
use App\ShopCore\Shop;
use App\Telegram\UserCommand;

class MessageHandler extends BaseMessageHandler
{

    public function places()
    {
        $command = $this->user->getLastCommand($this->token);

        /** @var Shop $shop */
        $shop = Shop::find($command->data['shop_id']);

        if (!$shop) {
            response('ok');
        }

        UserCommand::create($this->user, $this->token, CommandEnum::SHOP_PLACES, ['shop_id' => $shop->id]);

        return [
            '' => ''
        ];
    }

}
