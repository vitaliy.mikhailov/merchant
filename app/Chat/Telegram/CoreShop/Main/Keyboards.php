<?php

namespace App\Chat\Telegram\CoreShop\Main;

use App\Chat\Telegram\Base\BaseKeyboard;
use App\Enums\StatusEnum;
use App\Geo\Language;
use App\ShopCore\Shop;

class Keyboards extends BaseKeyboard
{

    public static function getLangKeyboard()
    {
        $languages = Language::where('status', StatusEnum::ENABLED)->get();

        $keyboard = [];

        /** @var Language $language */
        foreach ($languages as $language) {
            $keyboard[] = [
                'text' => $language->emoji_flag . ' ' . $language->name
            ];
        }

        $keyboard = array_chunk($keyboard, 2);

        return json_encode(
            [
                'keyboard' => $keyboard,
                'one_time_keyboard' => false,
                'resize_keyboard' => true
            ]
        );
    }

    public static function getMainMenu()
    {
        return json_encode(
            [
                'keyboard' =>
                    [
                        [
                            [
                                'text' => trans('buttons.main.shops')
                            ]
                        ],
                        [
                            [
                                'text' => trans('buttons.main.pricePolicy')
                            ],
                            [
                                'text' => trans('buttons.main.disclaimer')
                            ]
                        ],
                        [
                            [
                                'text' => trans('buttons.main.articles')
                            ]
                        ],
                    ],
                'one_time_keyboard' => false,
                'resize_keyboard' => true
            ]
        );
    }

    public static function getArticlesNavigation($articles)
    {
        $page = $articles['page'];
        $totalPage = $articles['total'];

        if ($totalPage < 3) {
            if ($page == 1) {
                return json_encode(
                    [
                        'inline_keyboard' => [
                            [
                                [
                                    'text' => trans('buttons.main.empty'),
                                    'callback_data' => json_encode(['command' => 'main.noData'])
                                ],
                                [
                                    'text' => trans('buttons.main.nextPage'),
                                    'callback_data' => json_encode(['command' => 'main.getPage', 'data' => $page + 1])
                                ],
                            ]
                        ]
                    ]
                );
            } elseif ($page == $totalPage) {
                return json_encode(
                    [
                        'inline_keyboard' => [
                            [
                                [
                                    'text' => trans('buttons.main.previousPage'),
                                    'callback_data' => json_encode(['command' => 'main.getPage', 'data' => $page - 1])
                                ],
                                [
                                    'text' => trans('buttons.main.empty'),
                                    'callback_data' => json_encode(['command' => 'main.noData'])
                                ],
                            ]
                        ]
                    ]
                );
            }
        } else {
            if ($page == 1) {
                return json_encode(
                    [
                        'inline_keyboard' => [
                            [
                                [
                                    'text' => trans('buttons.main.empty'),
                                    'callback_data' => json_encode(['command' => 'main.noData'])
                                ],
                                [
                                    'text' => trans('buttons.main.empty'),
                                    'callback_data' => json_encode(['command' => 'main.noData'])
                                ],
                                [
                                    'text' => trans('buttons.main.nextPage'),
                                    'callback_data' => json_encode(['command' => 'main.getPage', 'data' => $page + 1])
                                ],
                                [
                                    'text' => trans('buttons.main.lastPage'),
                                    'callback_data' => json_encode(['command' => 'main.getPage', 'data' => $totalPage])
                                ]
                            ]
                        ]
                    ]
                );
            } elseif ($page == $totalPage) {
                return json_encode(
                    [
                        'inline_keyboard' => [
                            [
                                [
                                    'text' => trans('buttons.main.firstPage'),
                                    'callback_data' => json_encode(['command' => 'main.getPage', 'data' => 1])
                                ],
                                [
                                    'text' => trans('buttons.main.previousPage'),
                                    'callback_data' => json_encode(['command' => 'main.getPage', 'data' => $page - 1])
                                ],
                                [
                                    'text' => trans('buttons.main.empty'),
                                    'callback_data' => json_encode(['command' => 'main.noData'])
                                ],
                                [
                                    'text' => trans('buttons.main.empty'),
                                    'callback_data' => json_encode(['command' => 'main.noData'])
                                ],
                            ]
                        ]
                    ]
                );
            } else {
                return json_encode(
                    [
                        'inline_keyboard' => [
                            [
                                [
                                    'text' => trans('buttons.main.firstPage'),
                                    'callback_data' => json_encode(['command' => 'main.getPage', 'data' => 1])
                                ],
                                [
                                    'text' => trans('buttons.main.previousPage'),
                                    'callback_data' => json_encode(['command' => 'main.getPage', 'data' => $page - 1])
                                ],
                                [
                                    'text' => trans('buttons.main.nextPage'),
                                    'callback_data' => json_encode(['command' => 'main.getPage', 'data' => $page + 1])
                                ],
                                [
                                    'text' => trans('buttons.main.lastPage'),
                                    'callback_data' => json_encode(['command' => 'main.getPage', 'data' => $totalPage])
                                ],
                            ]
                        ]
                    ]
                );
            }
        }
    }


}
