<?php


namespace App\Chat\Telegram\CoreShop\Main;


use App\Chat\Telegram\Base\BaseCallbackQueryHandler;
use App\Chat\Telegram\Base\TelegramHandler;
use App\Enums\TelegramEnum;
use App\ShopCore\Article;

class CallbackQueryHandler extends BaseCallbackQueryHandler
{

    public function getPage()
    {
        $data = json_decode($this->data['callback_query']['data'], true);

        $articles = Article::getArticles($data['data']);

        $text = '';

        /** @var Article $article */
        foreach ($articles['current'] as $article) {
            $text .= "\xF0\x9F\x93\xB0 [" . $article->title . '](' . Article::BASE_PATH . $article->link . ')' . PHP_EOL;
            if (!is_null($article->short_content)) {
                $text .= $article->short_content . PHP_EOL;
            }
            $text .= PHP_EOL;
        }

        $text .= trans('messages.page_msg', ['page' => $articles['page'], 'total' => $articles['total']]);

        $responseTextParams = [
            'chat_id' => $this->data['callback_query']['message']['chat']['id'],
            'message_id' => $this->data['callback_query']['message']['message_id'],
            'text' => $text,
            'disable_web_page_preview' => true,
            'parse_mode' => 'Markdown'
        ];

        $responseKeyboardParams = [
            'chat_id' => $this->data['callback_query']['message']['chat']['id'],
            'message_id' => $this->data['callback_query']['message']['message_id'],
            'reply_markup' => Keyboards::getArticlesNavigation($articles)
        ];

        TelegramHandler::requestTelegram($this->token,TelegramEnum::EDIT_MESSAGE_TEXT, $responseTextParams);
        TelegramHandler::requestTelegram($this->token,TelegramEnum::EDIT_MESSAGE_REPLY_MARKUP, $responseKeyboardParams);
        return ['status' => 'ok'];
    }


}
