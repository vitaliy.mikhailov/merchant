<?php

namespace App\Chat\Telegram\CoreShop\Main;

use App\Chat\Telegram\Base\BaseMessageHandler;
use App\Enums\CommandEnum;
use App\ShopCore\Article;
use App\Telegram\TelegramUser;
use App\Telegram\UserCommand;
use Illuminate\Support\Facades\App;
use \App\Chat\Telegram\CoreShop\Shop\MessageHandler as ShopMessageHandler;

class MessageHandler extends BaseMessageHandler
{

    public function start(): array
    {
        //$messageText = $this->request['message']['text'];

        if (!is_null($this->user) && is_null($this->user->language_code)) {
            $messageParams = [
                'text' => trans('messages.lang_menu_msg'),
                'reply_markup' => Keyboards::getLangKeyboard()
            ];
        } elseif (!is_null($this->user)) {
            $messageParams = $this->mainMenu();
        } else {
            //$refCode = str_replace('/start ', '', $messageText);

            $username = (isset($this->request['message']['from']['username']) && !is_null(
                    $this->request['message']['from']['username']
                )) ? $this->request['message']['from']['username'] : '';
            $userData = [
                'username' => $username,
                'telegram_id' => $this->request['message']['from']['id'],
                'first_name' => $this->request['message']['from']['first_name'],
                'last_name' => isset($this->request['message']['from']['last_name']) ? $this->request['message']['from']['last_name'] : null,
            ];

            TelegramUser::create($userData, $this->token);
            $messageParams = [
                'text' => trans('messages.lang_menu_msg'),
                'reply_markup' => Keyboards::getLangKeyboard()
            ];
        }
        return $messageParams;
    }

    public function setLanguage(): array
    {
        if (App::getLocale() != $this->user->language_code) {
            $this->user->language_code = App::getLocale();
            $this->user->save();
        }
        return $this->mainMenu();
    }

    public function mainMenu(): array
    {
        UserCommand::rejectAllCommands($this->user, $this->token);

        $text = $this->message . trans('messages.main_menu_msg');
        return [
            'text' => $text,
            'reply_markup' => Keyboards::getMainMenu()
        ];
    }

    public function disclaimer(): array
    {
        UserCommand::create($this->user, $this->token, CommandEnum::DISCLAIMER);

        return [
            'text' => trans('messages.disclaimer_msg'),
            'reply_markup' => Keyboards::back()
        ];
    }

    public function pricePolicy(): array
    {
        UserCommand::create($this->user, $this->token, CommandEnum::PRICE_POLICE);

        return [
            'text' => trans('messages.price_policy_msg'),
            'reply_markup' => Keyboards::back()
        ];
    }

    public function shops(): array
    {
        $shopHandler = new ShopMessageHandler($this->request, $this->token);
        return $shopHandler->shops();
    }

    public function articles(): array
    {
        UserCommand::create($this->user, $this->token, CommandEnum::ARTICLES);

        $articles = Article::getArticles();

        if ($articles['total'] == 0) {
            return [
                'text' => trans('errors.articles_not_found'),
                'reply_markup' => Keyboards::back()
            ];
        }

        $text = '';

        /** @var Article $article */
        foreach ($articles['current'] as $article) {
            $text .= "\xF0\x9F\x93\xB0 [" . $article->title . '](' . Article::BASE_PATH . $article->link . ')' . PHP_EOL;
            if (!is_null($article->short_content)) {
                $text .= $article->short_content . PHP_EOL;
            }
            $text .= PHP_EOL;
        }

        $text .= trans('messages.page_msg', ['page' => $articles['page'], 'total' => $articles['total']]);

        return [
            'text' => $text,
            'reply_markup' => Keyboards::getArticlesNavigation($articles),
            'disable_web_page_preview' => true
        ];
    }


}
