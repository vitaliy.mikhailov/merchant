<?php

namespace App\Chat\Telegram\CoreShop\Shop;

use App\Chat\Telegram\Base\BaseKeyboard;
use App\Enums\StatusEnum;
use App\Geo\Language;
use App\ShopCore\Shop;

class Keyboards extends BaseKeyboard
{

    public static function shops($shops)
    {
        $shopKeyboard = [];

        /** @var Shop $shop */
        foreach ($shops as $shop) {
            $shopKeyboard[] = [
                'text' => '#' . $shop->id . ' ' . $shop->name
            ];
        }

        $keyboard = [
            [
                [
                    'text' => trans('buttons.shop.createShop')
                ]
            ],
            [
                [
                    'text' => trans('buttons.shop.enableAllShops')
                ],
                [
                    'text' => trans('buttons.shop.disableAllShops')
                ]
            ],
            $shopKeyboard,
            [
                [
                    'text' => trans('buttons.main.back')
                ]
            ]
        ];

        return json_encode(
            [
                'keyboard' => $keyboard,
                'one_time_keyboard' => false,
                'resize_keyboard' => true
            ]
        );
    }

    public static function oneShop(Shop $shop)
    {
        $placeButton = $shop->place_count > 0 ? trans('buttons.place.places') : trans('buttons.place.addPlace');

        $categoryButton = $shop->category_count > 0 ? trans('buttons.category.categories') : trans(
            'buttons.category.addCategory'
        );

        $personalButton = $shop->personal_count > 0 ? trans('buttons.personal.personal') : trans(
            'buttons.personal.addWorker'
        );

        return json_encode(
            [
                'keyboard' =>
                    [
                        [
                            [
                                'text' => $placeButton
                            ],
                            [
                                'text' => $categoryButton
                            ],
                        ],
                        [
                            [
                                'text' => $personalButton
                            ]
                        ],
                        [
                            [
                                'text' => trans('buttons.shop.enableShop')
                            ],
                            [
                                'text' => trans('buttons.shop.disableShop')
                            ]
                        ],
                        [
                            [
                                'text' => trans('buttons.main.back')
                            ],
                            [
                                'text' => trans('buttons.main.mainMenu')
                            ]
                        ],
                    ],
                'one_time_keyboard' => false,
                'resize_keyboard' => true
            ]
        );
    }

}
