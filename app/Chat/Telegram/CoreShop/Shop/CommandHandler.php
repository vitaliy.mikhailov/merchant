<?php

namespace App\Chat\Telegram\CoreShop\Shop;

use App\Chat\Telegram\Base\BaseCommandHandler;
use App\Chat\Telegram\Base\TelegramHandler;
use App\Enums\BotTypeEnum;
use App\Enums\CommandEnum;
use App\Enums\ShopStatusEnum;
use App\Enums\TelegramEnum;
use App\Enums\UserTypeEnum;
use App\ShopCore\Bot;
use App\ShopCore\Shop;
use App\ShopCore\UserBotTie;
use App\Telegram\UserCommand;
use App\Chat\Telegram\CoreShop\Main\MessageHandler as MainMessageHandler;

class CommandHandler extends BaseCommandHandler
{

    public function getOneShop(): array
    {
        if (!is_null($this->request['message']['text'])) {
            $id = substr($this->request['message']['text'], strpos($this->request['message']['text'], '#') + 1);
            $id = substr($id, 0, strpos($id, ' '));

            /** @var Shop $shop */
            $shop = Shop::where('id', $id)
                ->where('author_id', $this->user->id)
                ->first();

            if (is_null($shop)) {
                return [
                    'text' => 'errors.shop_not_found'
                ];
            }

            UserCommand::create($this->user, $this->token, CommandEnum::GET_ONE_SHOP, ['shop_id' => $shop->id]);

            $text = $shop->getOneShopText();

            return [
                'text' => $text,
                'reply_markup' => Keyboards::oneShop($shop)
            ];
        } else {
            return [
                'text' => 'errors.can_not_use_this_data'
            ];
        }
    }

    public function setShopTokens(): array
    {
        $tokenRegexp = '/[0-9]{9}:[a-zA-Z0-9_-]{35}/';
        if (!is_null($this->request['message']['text']) && preg_match(
                $tokenRegexp,
                $this->request['message']['text']
            )) {
            $message = $this->request['message']['text'];
            if (Bot::where('token', $message)->exists()) {
                return [
                    'text' => trans('errors.can_not_use_this_token')
                ];
            }

            $botData = TelegramHandler::requestTelegram($message, TelegramEnum::GET_ME);
            if (isset($botData['status_code'])) {
                return [
                    'text' => 'errors.invalid_bot_token'
                ];
            }
            $botData = $botData['result'];

            $lastCommand = $this->user->getLastCommand($this->token);
            $commandData = $lastCommand->data;

            if (!isset($commandData['shop'])) {
                $commandData['shop'] = [
                    'type' => BotTypeEnum::SHOP,
                    'telegram_id' => $botData['id'],
                    'first_name' => $botData['first_name'],
                    'username' => $botData['username'],
                    'token' => $message,
                ];
                UserCommand::create($this->user, $this->token, CommandEnum::SET_SHOP_TOKENS, $commandData);

                return [
                    'text' => trans('messages.create_shop_step_two')
                ];
            } else {
                if ($commandData['shop']['token'] == $message) {
                    return [
                        'text' => trans('errors.can_not_use_this_token')
                    ];
                }

                $requestParams = [
                    'chat_id' => $this->user->telegram_id
                ];

                $shopResponse = TelegramHandler::requestTelegram(
                    $commandData['shop']['token'],
                    TelegramEnum::GET_CHAT,
                    $requestParams
                );
                if (isset($shopResponse['status_code'])) {
                    return [
                        'text' => trans('errors.shop_bot_is_disable')
                    ];
                }

                $chatResponse = TelegramHandler::requestTelegram($message, TelegramEnum::GET_CHAT, $requestParams);
                if (isset($chatResponse['status_code'])) {
                    return [
                        'chat_id' => $this->user->telegram_id,
                        'text' => trans('errors.shop_bot_is_disable')
                    ];
                }

                $commandData['chat'] = [
                    'type' => BotTypeEnum::CHAT,
                    'telegram_id' => $botData['id'],
                    'first_name' => $botData['first_name'],
                    'username' => $botData['username'],
                    'token' => $message,
                ];
                UserCommand::create($this->user, $this->token, CommandEnum::SET_SHOP_TOKENS, $commandData);

                self::makeShop($commandData);

                UserCommand::create($this->user, $this->token, CommandEnum::GET_ONE_SHOP);

                $messageHandler = new MainMessageHandler(
                    $this->request,
                    $this->token,
                    'messages.creating_new_shop_successful'
                );
                return $messageHandler->shops();
            }
        } else {
            return [
                'text' => 'errors.can_not_use_this_data'
            ];
        }
    }

    private function makeShop($data)
    {
        $shopData = [
            'author_id' => $this->user->id,
            'name' => $data['shop']['first_name']
        ];
        $shop = Shop::create($shopData);

        $data['shop']['shop_id'] = $shop->id;
        $shopBot = Bot::create($data['shop']);

        $data['chat']['shop_id'] = $shop->id;
        $chatBot = Bot::create($data['chat']);

        TelegramHandler::requestTelegram(
            $data['shop']['token'],
            TelegramEnum::SET_WEBHOOK,
            ['url' => (config('app.url') . $data['shop']['token'])]
        );

        TelegramHandler::requestTelegram(
            $data['chat']['token'],
            TelegramEnum::SET_WEBHOOK,
            ['url' => (config('app.url') . $data['chat']['token'])]
        );

        $tieUser = new UserBotTie();
        $tieUser->bot_id = $shopBot->id;
        $tieUser->user_id = $this->user->id;
        $tieUser->role = UserTypeEnum::ADMINISTRATOR;
        $tieUser->save();

        $tieUser = new UserBotTie();
        $tieUser->bot_id = $chatBot->id;
        $tieUser->user_id = $this->user->id;
        $tieUser->role = UserTypeEnum::ADMINISTRATOR;
        $tieUser->save();
    }

}
