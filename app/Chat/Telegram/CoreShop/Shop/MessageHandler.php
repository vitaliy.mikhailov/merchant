<?php

namespace App\Chat\Telegram\CoreShop\Shop;

use App\Chat\Telegram\Base\BaseMessageHandler;
use App\Chat\Telegram\Base\TelegramHandler;
use App\Enums\BotTypeEnum;
use App\Enums\CommandEnum;
use App\Enums\ShopStatusEnum;
use App\Enums\TelegramEnum;
use App\ShopCore\Bot;
use App\ShopCore\Shop;
use App\Telegram\UserCommand;
use App\Chat\Telegram\CoreShop\Place\MessageHandler as PlaceMessageHandler;

class MessageHandler extends BaseMessageHandler
{

    public function shops(): array
    {
        UserCommand::create($this->user, $this->token, CommandEnum::GET_ONE_SHOP);
        $text = $this->message . PHP_EOL . PHP_EOL . trans('messages.shops_msg');

        if ($this->user->shops->count() == 0) {
            $text .= PHP_EOL . PHP_EOL . trans('messages.if_shops_is_empty_msg');
        }

        return [
            'text' => $text,
            'reply_markup' => Keyboards::shops($this->user->shops)
        ];
    }

    public function createShop()
    {
        UserCommand::create($this->user, $this->token, CommandEnum::SET_SHOP_TOKENS);

        $messageParams = [
            'text' => trans('messages.create_shop_step_one'),
            'chat_id' => $this->user->telegram_id,
            'parse_mode' => 'Markdown',
            'reply_markup' => Keyboards::back()
        ];

        TelegramHandler::requestTelegram($this->token, TelegramEnum::SEND_MESSAGE, $messageParams);

        sleep(2);

        /** @var Bot $bot */
        $bot = Bot::where('type', BotTypeEnum::CHAT)
            ->whereNull('shop_id')
            ->first();

        $url = 'https://t.me/' . $bot->username . '?start';

        return [
            'text' => trans('messages.create_shop_step_one_info'),
            'reply_markup' => Keyboards::supportButton($url)
        ];
    }

    public function places()
    {
        $shopHandler = new PlaceMessageHandler($this->request, $this->token);
        return $shopHandler->places();
    }

    public function enableAllShops()
    {
        Shop::enableAllShop($this->user);

        $messageHandler = new MessageHandler(
            $this->request,
            $this->token,
            'messages.all_shops_was_enabled'
        );
        return $messageHandler->shops();
    }

    public function disableAllShops()
    {
        Shop::whereIn('status', [ShopStatusEnum::ENABLED])->update(['status' => ShopStatusEnum::DISABLED]);

        $messageHandler = new MessageHandler(
            $this->request,
            $this->token,
            'messages.all_shops_was_disabled'
        );
        return $messageHandler->shops();
    }

    public function enableShop()
    {
        $command = $this->user->getLastCommand($this->token);

        /** @var Shop $shop */
        $shop = Shop::find($command->data['shop_id']);

        if (!$shop) {
            response('ok');
        }

        if ($shop->places->count() == 0) {
            return [
                'text' => trans('errors.shop_places_is_empty')
            ];
        }

        if ($shop->categories->count() == 0) {
            return [
                'text' => trans('errors.shop_categories_is_empty')
            ];
        }

        $shop->status = ShopStatusEnum::ENABLED;

        return [
            'text' => trans('messages.shop_was_enabled', ['shopName' => $shop->name])
        ];
    }

    public function disableShop()
    {
        $command = $this->user->getLastCommand($this->token);

        /** @var Shop $shop */
        $shop = Shop::find($command->data['shop_id']);

        if (!$shop) {
            response('ok');
        }

        return [
            'text' => trans('messages.shop_was_disabled', ['shopName' => $shop->name])
        ];
    }

}
