<?php

namespace App\Http\Controllers;

use App\Chat\Telegram\Base\TelegramHandler;
use App\Jobs\AnswerTelegramRequest;
use Illuminate\Http\Request;

class TelegramController extends Controller
{
    public function index(Request $request)
    {
        if (config('app.testing')) {
            $telegram = new TelegramHandler($request->all(), $request->path());
            $telegram->parseTelegramRequest();
        } else {
            AnswerTelegramRequest::dispatch($request->all())->onQueue('High');
        }
        return response('ok');
    }
}
