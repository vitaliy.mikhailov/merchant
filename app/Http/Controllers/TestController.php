<?php

namespace App\Http\Controllers;

use App\Chat\Telegram\Base\TelegramHandler;
use App\Chat\Telegram\CoreShop\Main\Keyboards;
use App\Enums\TelegramEnum;
use App\ShopCore\Article;
use App\Telegram\UserCommand;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Request;

class TestController extends Controller
{

    public function test(Request $request)
    {
        $data = json_decode('{"message": {"chat": {"id": 223801097, "type": "private", "username": "vis1712_kz", "last_name": "Mikhailov", "first_name": "Vitaliy"}, "date": 1586373795, "from": {"id": 223801097, "is_bot": false, "username": "vis1712_kz", "last_name": "Mikhailov", "first_name": "Vitaliy", "language_code": "ru"}, "text": "✅ Включить магазин", "message_id": 347}, "update_id": 788897699}', true);
        $telegram = new TelegramHandler($data, '1096564608:AAEL6NUSyIfUC56ix_1Rt-qAXYn7ro88HuE');
        dd($telegram->parseTelegramRequest());
    }
}
