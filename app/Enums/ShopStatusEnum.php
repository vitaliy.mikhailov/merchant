<?php

namespace App\Enums;


class ShopStatusEnum extends BaseEnum
{
    public const DISABLED = 0;

    public const ENABLED = 1;

    public const DELETED = 2;

    public const REJECTED_BY_PLATFORM = 3;
}
