<?php

namespace App\Enums;

use ReflectionClass;

/**
 * Class BaseEnum
 * @package App\Enums
 *
 * @method $getConstants
 */
class BaseEnum
{

    public static function all() {
        return (new ReflectionClass(static::class))->getConstants();
    }

}
