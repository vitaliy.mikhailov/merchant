<?php

namespace App\Enums;

class UserTypeEnum extends BaseEnum
{
    public const ADMINISTRATOR = 'administrator';

    public const MANAGER = 'manager';

    public const USER = 'user';
}
