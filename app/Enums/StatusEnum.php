<?php

namespace App\Enums;


class StatusEnum extends BaseEnum
{
    public const DISABLED = 0;

    public const ENABLED = 1;

    public const REJECTED = 2;
}