<?php

namespace App\Enums;


class TransactionTypeEnum extends BaseEnum
{
    public const PAYMENT = 0;

    public const COMMISSION = 1;

    public const CASHOUT = 2;
}
