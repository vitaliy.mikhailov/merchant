<?php

namespace App\Enums;

class ContinentEnum extends BaseEnum
{
    public const AFRICA = 'AF';
    public const ASIA = 'AS';
    public const EUROPE = 'EU';
    public const NORTH_AMERICA = 'NA';
    public const SOUTH_AMERICA = 'SA';
    public const OCEANIA = 'OC';
    public const ANTARCTICA = 'AN';

}