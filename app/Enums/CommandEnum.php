<?php

namespace App\Enums;

class CommandEnum extends BaseEnum
{

    public const DISCLAIMER = 'main.disclaimer';

    public const PRICE_POLICE = 'main.pricePolice';

    public const ARTICLES = 'main.articles';

    public const GET_ONE_SHOP = 'shop.getOneShop';

    public const SET_SHOP_TOKENS = 'shop.setShopTokens';

    public const SHOP_PLACES = 'shop.places';

}
