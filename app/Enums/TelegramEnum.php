<?php

namespace App\Enums;


class TelegramEnum extends BaseEnum
{
    public const PARSE_MODE = 'Markdown';

    public const SEND_MESSAGE = 'sendMessage';

    public const EDIT_MESSAGE_REPLY_MARKUP = 'editMessageReplyMarkup';

    public const EDIT_MESSAGE_TEXT = 'editMessageText';

    public const GET_UPDATES = 'getUpdates';

    public const GET_FILE = 'getFile';

    public const GET_CHAT = 'getChat';

    public const ANSWER_CALLBACK_QUERY = 'answerCallbackQuery';

    public const DELETE_MESSAGE = 'deleteMessage';

    public const GET_ME = 'getMe';

    public const SET_WEBHOOK = 'setWebhook';
}
