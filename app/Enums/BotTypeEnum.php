<?php

namespace App\Enums;

class BotTypeEnum extends BaseEnum
{

    public const SHOP = 'shop';

    public const CHAT = 'chat';

    public const DETAIL = 'detail';

}
