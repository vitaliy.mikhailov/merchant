<?php

namespace App\Enums;


class TransactionStatusEnum extends BaseEnum
{
    public const NEW = 0;

    public const IN_PROCESS = 1;

    public const COMPLETED = 2;

    public const CANCELED = 3;
}
