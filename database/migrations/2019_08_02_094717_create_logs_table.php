<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('create schema "system";');

        Schema::create('system.logs', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('Идентификатор');
            $table->integer('user_id')->nullable()->comment('Пользователь');
            $table->jsonb('request')->nullable()->comment('Запрос');
            $table->integer('response_code')->nullable()->comment('Код ответа');
            $table->jsonb('response')->nullable()->comment('Ответ');
            $table->timestamps();
            $table->smallInteger('status')->default(1)->comment('Статус');
        });

        DB::statement("comment on schema system is 'Данные системы'");

        DB::statement("comment on table system.logs is 'Логирование'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('system.logs');
        DB::statement('drop schema "system" cascade;');
    }
}
