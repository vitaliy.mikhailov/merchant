<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('core.articles', function (Blueprint $table) {
            $table->id()->comment('Идентификатор');
            $table->text('title')->comment('Название статьи');
            $table->text('short_content')->nullable()->comment('Краткое содержание');
            $table->text('link')->comment('Ссылка');
            $table->timestamp('publication_date')->useCurrent()->comment('Дата публикации');
            $table->timestamps();
            $table->integer('status')->default(1)->comment('Статус');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('core.articles');
    }
}
