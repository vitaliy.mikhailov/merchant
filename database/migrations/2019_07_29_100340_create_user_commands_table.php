<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserCommandsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bot.user_commands', function (Blueprint $table) {
            $table->increments('id')->comment('Идентификатор');
            $table->integer('user_id')->comment('Пользователь');
            $table->integer('bot_id')->comment('Бот');
            $table->string('type', 64)->comment('Состояние');
            $table->jsonb('data')->nullable()->comment('Дополнительные данные');
            $table->timestamps();
            $table->smallInteger('status')->default(1)->comment('Статус');

            $table->foreign('user_id')->references('id')->on('bot.telegram_users');
            $table->foreign('bot_id')->references('id')->on('core.bots');
        });

        DB::statement("comment on table bot.user_commands is 'Команды пользователей'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bot.user_commands');
    }
}
