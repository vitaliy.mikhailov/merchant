<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlacesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('geo.places', function (Blueprint $table) {
            $table->increments('id')->comment('Идентификатор');
            $table->string('country_code', 2)->comment('Страна');
            $table->string('name', 200)->comment('Имя');
            $table->string('ascii_name', 200)->nullable()->comment('Имя на латинице');
            $table->decimal('latitude', 12, 8)->comment('Широта');
            $table->decimal('longitude', 12, 8)->comment('Долгота');
            $table->smallInteger('elevation')->nullable()->comment('Высота над уровнем моря');
            $table->string('feature_code', 1)->comment('Тип');
            $table->string('feature_class', 10)->comment('Класс');
            $table->integer('population')->comment('Население');
            $table->string('time_zone', 40)->comment('Таймзона');
            $table->timestamps();
            $table->smallInteger('status')->default(1)->comment('Статус');

            $table->foreign('country_code')->references('iso')->on('geo.countries');
            $table->foreign('time_zone')->references('time_zone')->on('geo.time_zones');
        });

        DB::statement("comment on table geo.places is 'Города'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('geo.places');
    }
}
