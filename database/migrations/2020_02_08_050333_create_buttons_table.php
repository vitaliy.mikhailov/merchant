<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateButtonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     * @property $id
     * @property $language
     * @property $command
     * @property $text
     * @property $status
     */
    public function up()
    {
        Schema::create('system.buttons', function (Blueprint $table) {
            $table->id()->comment('Идентификатор');
            $table->string('section')->nullable()->comment('Раздел бота');
            $table->string('command', 128)->comment('Комманда');
            $table->smallInteger('status')->default(1)->comment('Статус');
        });

        DB::statement("comment on table system.buttons is 'Кнопки бота'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('system.buttons');
    }
}
