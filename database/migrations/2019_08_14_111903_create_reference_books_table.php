<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReferenceBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('system.reference_books', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('Идентификатор');
            $table->string('name')->comment('Название справочника');
            $table->string('entity')->comment('Сущность');
            $table->timestamps();
            $table->smallInteger('status')->default(1)->comment('Статус');
        });

        DB::statement("comment on table system.reference_books is 'Все справочники системы'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('system.reference_books');
    }

}
