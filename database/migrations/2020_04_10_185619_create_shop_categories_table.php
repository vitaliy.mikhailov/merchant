<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShopCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('core.shop_categories', function (Blueprint $table) {
            $table->id();
            $table->integer('shop_id')->comment('Магазин');
            $table->integer('place_shop_tie_id')->comment('Магазин');
            $table->string('name')->comment('Название');
            $table->timestamps();
            $table->smallInteger('status')->default(1)->comment('Статус');

            $table->foreign('shop_id')->references('id')->on('core.shops');
            $table->foreign('place_shop_tie_id')->references('id')->on('core.place_shop_ties');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('core.shop_categories');
    }
}
