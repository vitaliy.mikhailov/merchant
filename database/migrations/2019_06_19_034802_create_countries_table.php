<?php

use App\Enums\ContinentEnum;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCountriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */


    /**
     *
     */
    public function up()
    {

        DB::statement('create schema "geo";');

        Schema::create('geo.countries', function (Blueprint $table) {
            $table->integer('id')->comment('Идентификатор');
            $table->string('iso', 2)->comment('Код страны 2 символа');
            $table->string('iso3', 3)->nullable()->comment('Код страны 3 символа');
            $table->string('iso_numeric', 3)->nullable()->comment('Численный код страны');
            $table->string('fips', 2)->nullable();
            $table->text('name')->comment('Название страны');
            $table->text('capital')->nullable()->comment('Столица');
            $table->decimal('area', 16, 2)->comment('Площадь');
            $table->bigInteger('population')->comment('Население');
            $table->enum('continent', [ContinentEnum::all()])->comment('Континент');
            $table->string('tld', 25)->nullable();
            $table->string('currency_code', 3)->nullable()->comment('Валюта');
            $table->string('phone_prefix', 25)->nullable()->comment('Префикс телефона');
            $table->jsonb('boundaries')->default(json_encode([null]))->comment('Границы');
            $table->smallInteger('sort')->nullable()->comment('Сортировка, если необходимо');
            $table->timestamps();
            $table->smallInteger('status')->default(1)->comment('Статус');

            $table->unique('id');
            $table->unique('iso');
            $table->unique('iso3');
            $table->unique('iso_numeric');
        });

        DB::statement("comment on schema geo is 'Географические данные'");

        DB::statement("comment on table geo.countries is 'Страны'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('geo.countries');
        DB::statement('drop schema "geo" cascade;');
    }
}
