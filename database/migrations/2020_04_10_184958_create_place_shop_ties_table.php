<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlaceShopTiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('core.place_shop_ties', function (Blueprint $table) {
            $table->id();
            $table->integer('shop_id')->comment('Магазин');
            $table->integer('place_id')->comment('Город');
            $table->timestamps();
            $table->smallInteger('status')->default(1)->comment('Статус');

            $table->foreign('shop_id')->references('id')->on('core.shops');
            $table->foreign('place_id')->references('id')->on('geo.places');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('core.place_shop_ties');
    }
}
