<?php

use App\Enums\BotTypeEnum;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateBotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('core.bots', function (Blueprint $table) {
            $table->id();
            $table->enum('type', BotTypeEnum::all())->comment('Тип бота');
            $table->integer('shop_id')->nullable()->comment('Магазин');
            $table->integer('telegram_id')->comment('ID телеграм');
            $table->string('first_name')->nullable()->comment('Имя бота');
            $table->string('username')->comment('Юзернейм');
            $table->string('token')->comment('Токен бота');
            $table->timestamps();
            $table->smallInteger('status')->default(1)->comment('Статус');

            $table->foreign('shop_id')->references('id')->on('core.shops');
        });

        DB::statement("comment on table core.bots is 'Боты'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('core.bots');
    }
}
