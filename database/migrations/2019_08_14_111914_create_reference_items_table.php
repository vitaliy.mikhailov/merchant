<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReferenceItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('system.reference_items', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('Идентификатор');
            $table->integer('parent_id')->nullable()->comment('Ссылка на вышесстоящий элемент');
            $table->integer('reference_book_id')->comment('Ссылка на спрвочник');
            $table->string('entity')->comment('Сущность');
            $table->integer('sort')->nullable()->comment('Порядок сортировки');
            $table->timestamps();
            $table->smallInteger('status')->default(1)->comment('Статус');

            $table->foreign('parent_id')->references('id')->on('system.reference_items');
            $table->foreign('reference_book_id')->references('id')->on('system.reference_books');
        });

        DB::statement("comment on table system.reference_items is 'Все элементы справочников'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('system.reference_items');
    }
}
