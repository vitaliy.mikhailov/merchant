<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateLanguagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('geo.languages', function (Blueprint $table) {
            $table->string('code', 2)->comment('Код языка');
            $table->string('name')->comment('Название');
            $table->string('emoji_flag')->comment('Смайлик');
            $table->timestamps();
            $table->smallInteger('status')->default(1)->comment('Статус');

            $table->primary('code');
        });

        DB::statement("comment on table geo.languages is 'Языки'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('geo.languages');
    }
}
