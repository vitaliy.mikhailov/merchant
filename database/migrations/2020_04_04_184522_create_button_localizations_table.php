<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateButtonLocalizationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('system.button_localizations', function (Blueprint $table) {
            $table->id()->comment('Идентификатор');
            $table->integer('command_id')->comment('Комманда');
            $table->string('language_code', 2)->default('ru')->comment('Локаль');
            $table->text('text')->comment('Надпись');

            $table->foreign('language_code')->references('code')->on('geo.languages');
            $table->foreign('command_id')->references('id')->on('system.buttons');
        });

        DB::statement("comment on table system.button_localizations is 'Локализация кнопок бота'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('system.button_localizations');
    }
}
