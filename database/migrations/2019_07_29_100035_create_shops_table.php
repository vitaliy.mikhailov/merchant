<?php

use App\Enums\ShopStatusEnum;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateShopsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        DB::statement('create schema "core";');

        Schema::create('core.shops', function (Blueprint $table) {
            $table->id();
            $table->integer('author_id')->comment('Автор магазина');
            $table->string('name')->comment('Название');
            $table->timestamps();
            $table->smallInteger('status')->default(ShopStatusEnum::DISABLED)->comment('Статус');
        });

        DB::statement("comment on schema core is 'Ядро'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('core.shops');
        DB::statement('drop schema "core" cascade;');
    }
}
