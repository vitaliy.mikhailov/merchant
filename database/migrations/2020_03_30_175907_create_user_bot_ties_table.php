<?php

use App\Enums\UserTypeEnum;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateUserBotTiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('core.user_bot_ties', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id')->comment('Пользователь');
            $table->integer('bot_id')->comment('Бот');
            $table->enum('role', UserTypeEnum::all())->comment('Роль');
            $table->jsonb('rules')->default(json_encode([]))->comment('Права');
            $table->timestamps();
            $table->smallInteger('status')->default(1)->comment('Статус');

            $table->unique(['user_id', 'bot_id']);

            $table->foreign('user_id')->references('id')->on('bot.telegram_users');
            $table->foreign('bot_id')->references('id')->on('core.bots');
        });

        DB::statement("comment on table core.user_bot_ties is 'Связь пользователя и бота'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('core.user_bot_ties');
    }
}
