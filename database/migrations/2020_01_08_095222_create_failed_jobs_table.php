<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFailedJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('system.failed_jobs', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('Идентификатор');
            $table->text('connection')->comment('Соединение');
            $table->text('queue')->comment('Очередь');
            $table->longText('payload')->comment('Задача');
            $table->longText('exception')->comment('Ошибка');
            $table->timestamp('failed_at')->useCurrent();
        });

        DB::statement("comment on table system.failed_jobs is 'Проваленные задания'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('system.failed_jobs');
    }
}
