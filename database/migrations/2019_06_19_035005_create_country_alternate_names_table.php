<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateCountryAlternateNamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('geo.country_alternate_names', function (Blueprint $table) {
            $table->increments('id')->comment('Идентификатор');
            $table->string('language_code')->comment('Язык');
            $table->integer('country_id')->comment('Страна');
            $table->string('name')->comment('Название страны');
            $table->timestamps();
            $table->smallInteger('status')->default(1)->comment('Статус');

            $table->foreign('country_id')->references('id')->on('geo.countries');
            $table->foreign('language_code')->references('code')->on('geo.languages');
        });

        DB::statement("comment on table geo.country_alternate_names is 'Локализованные названия стран'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('geo.country_alternate_names');
    }
}
