<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTimeZonesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('geo.time_zones', function (Blueprint $table) {
            $table->string('time_zone')->comment('Таймзона');
            $table->string('country_code', 2)->comment('Код страны');
            $table->decimal('GMT', 4, 2)->comment('Смещение');
            $table->timestamps();
            $table->smallInteger('status')->default(1)->comment('Таймзона');

            $table->unique('time_zone');
            $table->foreign('country_code')->references('iso')->on('geo.countries');
        });

        DB::statement("comment on table geo.time_zones is 'Часовые пояса'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('geo.time_zones');
    }
}
