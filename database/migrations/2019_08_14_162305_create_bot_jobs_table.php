<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBotJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('system.jobs', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('Идентификатор');
            $table->string('queue')->index()->comment('Очередь');
            $table->longText('payload')->comment('Задача');
            $table->unsignedTinyInteger('attempts')->comment('Попытки');
            $table->unsignedInteger('reserved_at')->nullable()->comment('Выполнено в');
            $table->unsignedInteger('available_at')->comment('Запланированно на');
            $table->unsignedInteger('created_at')->comment('Время создания');
        });

        DB::statement("comment on table system.jobs is 'Очередь для бота'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('system.jobs');
    }
}
