<?php

use App\Enums\UserTypeEnum;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTelegramUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('create schema "bot";');

        Schema::create('bot.telegram_users', function (Blueprint $table) {
            $table->increments('id')->comment('Идентификатор');
            $table->bigInteger('telegram_id')->comment('ID телеграм');
            $table->string('language_code', 2)->nullable()->comment('Локаль');
            $table->string('first_name', 128)->nullable()->comment('Имя');
            $table->string('last_name', 128)->nullable()->comment('Фамилия');
            $table->string('username', 128)->nullable()->comment('Юзернейм');
            $table->timestamps();
            $table->smallInteger('status')->default(1)->comment('Статус');

            $table->foreign('language_code')->references('code')->on('geo.languages');
            $table->unique('telegram_id');
        });

        DB::statement("comment on table bot.telegram_users is 'Пользователи телеграмм'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bot.telegram_users');
        DB::statement('drop schema "bot" cascade;');
    }
}
