<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreatePlaceAlternateNamesTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('geo.place_alternate_names', function (Blueprint $table) {
            $table->increments('id')->comment('Идентификатор');
            $table->string('language_code')->nullable()->comment('Язык');
            $table->integer('place_id')->comment('Место');
            $table->string('name')->comment('Название места');
            $table->timestamps();
            $table->smallInteger('status')->default(1)->comment('Статус');

            $table->foreign('place_id')->references('id')->on('geo.places');
            $table->foreign('language_code')->references('code')->on('geo.languages');
        });

        DB::statement("comment on table geo.place_alternate_names is 'Локализованные названия мест'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('geo.place_alternate_names');
    }
}
