<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PlacesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ini_set('memory_limit', '-1');

        $places = [];
        $placeNames = [];
        $count = 0;

        $file = file_get_contents(database_path('data' . DIRECTORY_SEPARATOR . 'place.txt'));
        $cities = explode("\n", $file);

        foreach ($cities as $city) {
            $count++;
            $currentCity = explode("\t", $city);

            if (trim($currentCity[1]) == 'les Escaldes') {
                $currentCity[0] = 3040051;
            }

            array_push($places, [
                'id' => intval($currentCity[0]),
                'name' => trim($currentCity[1]),
                'ascii_name' => empty($currentCity[2]) ? null : trim($currentCity[2]),
                'latitude' => trim($currentCity[4]),
                'longitude' => trim($currentCity[5]),
                'feature_code' => trim($currentCity[6]),
                'feature_class' => trim($currentCity[7]),
                'country_code' => trim($currentCity[8]),
                'population' => trim($currentCity[14]),
                'elevation' => empty($currentCity[15]) ? null : trim($currentCity[15]),
                'time_zone' => trim($currentCity[17]),
                'created_at' => date('Y-m-d H:i:s', time() - (6 * 60 * 60)),
                'updated_at' => date('Y-m-d H:i:s', time() - (6 * 60 * 60)),
            ]);

            if (!empty($currentCity[3])) {
                foreach (explode(',', trim($currentCity[3])) as $placeName) {
                    array_push($placeNames, [
                        'place_id' => intval($currentCity[0]),
                        'language_code' => null,
                        'name' => $placeName,
                        'created_at' => date('Y-m-d H:i:s', time() - (6 * 60 * 60)),
                        'updated_at' => date('Y-m-d H:i:s', time() - (6 * 60 * 60)),
                    ]);
                }
            }

            $placeNames[] = [
                'place_id' => intval($currentCity[0]),
                'language_code' => 'en',
                'name' => trim($currentCity[1]),
                'created_at' => date('Y-m-d H:i:s', time() - (6 * 60 * 60)),
                'updated_at' => date('Y-m-d H:i:s', time() - (6 * 60 * 60)),
            ];

            if ($count == 50) {
                DB::table('geo.places')->insert($places);
                DB::table('geo.place_alternate_names')->insert($placeNames);
                $places = [];
                $placeNames = [];
                $count = 0;
            }
        }
        DB::table('geo.places')->insert($places);
        DB::table('geo.place_alternate_names')->insert($placeNames);
    }
}
