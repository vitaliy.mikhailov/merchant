<?php

use App\Chat\Telegram\Base\TelegramHandler;
use App\Enums\BotTypeEnum;
use App\Enums\TelegramEnum;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BotsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $bots = [
            [
                'token' => '1096564608:AAEL6NUSyIfUC56ix_1Rt-qAXYn7ro88HuE',
                'type' => BotTypeEnum::SHOP,
            ],
            [
                'token' => '1116135052:AAH9TsNuDuGM2l_WxFQVrsVenuSENwHPAj8',
                'type' => BotTypeEnum::CHAT,
            ]
        ];

        $prepareData = [];

        foreach ($bots as $bot) {
            $botData = TelegramHandler::requestTelegram($bot['token'], TelegramEnum::GET_ME)['result'];
            TelegramHandler::requestTelegram($bot['token'], TelegramEnum::SET_WEBHOOK, ['url' => (config('app.url') . $bot['token'])]);

            $prepareData[] = [
                'type' => $bot['type'],
                'telegram_id' => $botData['id'],
                'first_name' => $botData['first_name'],
                'username' => $botData['username'],
                'token' => $bot['token'],
                'created_at' => date('Y-m-d H:i:s', time() - (6 * 60 * 60)),
                'updated_at' => date('Y-m-d H:i:s', time() - (6 * 60 * 60)),
            ];
        }

        DB::table('core.bots')->insert($prepareData);
    }
}
