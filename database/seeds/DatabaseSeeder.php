<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(LanguagesTableSeeder::class);
        $this->call(CountriesTableSeeder::class);
        $this->call(TimeZonesTableSeeder::class);
        $this->call(CommandTableSeeder::class);
        // $this->call(PlacesTableSeeder::class);
        $this->call(BotsTableSeeder::class);
        $this->call(ArticlesTableSeeder::class);
    }
}
