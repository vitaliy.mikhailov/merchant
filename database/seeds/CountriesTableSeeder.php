<?php

use App\Geo\Country;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CountriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $prepareData = [];
        $insert = [];
        $count = 0;

        $file = file_get_contents(database_path('data' . DIRECTORY_SEPARATOR . 'country.txt'));
        $countries = explode("\n", $file);

        foreach ($countries as $country) {

            $count++;

            $country = explode("\t", $country);

            $code = empty($country[10]) ? null : $country[10];

            array_push($prepareData, [
                'id' => empty($country[16]) ? null : $country[16],
                'iso' => empty($country[0]) ? null : $country[0],
                'iso3' => empty($country[1]) ? null : $country[1],
                'iso_numeric' => empty($country[2]) ? null : $country[2],
                'fips' => empty($country[3]) ? null : $country[3],
                'name' => $country[4],
                'capital' => empty($country[5]) ? null : $country[5],
                'area' => $country[6],
                'population' => $country[7],
                'continent' => $country[8],
                'tld' => empty($country[9]) ? null : $country[9],
                'currency_code' => $code,
                'phone_prefix' => empty($country[12]) ? null : $country[12],
                'boundaries' => json_encode(explode(',', $country[17])),
                'created_at' => date('Y-m-d H:i:s', time() - (6 * 60 * 60)),
                'updated_at' => date('Y-m-d H:i:s', time() - (6 * 60 * 60)),
            ]);

            $insert[] = [
                'language_code' => 'en',
                'country_id' => empty($country[16]) ? null : $country[16],
                'name' => $country[4],
                'created_at' => date('Y-m-d H:i:s', time() - (6 * 60 * 60)),
                'updated_at' => date('Y-m-d H:i:s', time() - (6 * 60 * 60)),
            ];

            if ($count == 10) {
                DB::table('geo.countries')->insert($prepareData);
                $count = 0;
                $prepareData =[];

                DB::table('geo.country_alternate_names')->insert($insert);
                $insert = [];
            }
        }

        DB::table('geo.countries')->insert($prepareData);

        DB::table('geo.country_alternate_names')->insert($insert);

    }
}
