<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ArticlesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [];

        for ($count = 0; $count < 35; $count++) {
            $data[] = [
                'title' => 'test_' . $count,
                'short_content' => 'short_content_' . $count,
                'link' => 'test-04-07-99'
            ];
        }

        DB::table('core.articles')->insert($data);
    }
}
