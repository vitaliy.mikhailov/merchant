<?php

use App\Geo\Language;
use App\Telegram\Button;
use App\Telegram\ButtonLocalization;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\File;

class CommandTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $languages = Language::all();

        /** @var Language $language */
        foreach ($languages as $language) {

            /** @var Button $button */
            $button = Button::firstOrCreate(['section' => 'main', 'command' => 'setLanguage']);

            $buttonLocalization = new ButtonLocalization();
            $buttonLocalization->command_id = $button->id;
            $buttonLocalization->language_code = $language->code;
            $buttonLocalization->text = $language->emoji_flag . ' ' . $language->name;
            $buttonLocalization->save();

            if (File::exists(resource_path('lang' . DIRECTORY_SEPARATOR . $language->code . DIRECTORY_SEPARATOR . 'buttons.php'))) {
                $localizationFilePath = resource_path('lang' . DIRECTORY_SEPARATOR . $language->code . DIRECTORY_SEPARATOR . 'buttons.php');
                $commandList = include_once($localizationFilePath);

                foreach ($commandList as $section => $commands) {
                    foreach ($commands as $command => $text) {
                        /** @var Button $button */
                        $button = Button::firstOrCreate(['section' => $section, 'command' => $command]);

                        $buttonLocalization = new ButtonLocalization();
                        $buttonLocalization->command_id = $button->id;
                        $buttonLocalization->language_code = $language->code;
                        $buttonLocalization->text = $text;
                        $buttonLocalization->save();
                    }
                }
            }
        }
    }
}
