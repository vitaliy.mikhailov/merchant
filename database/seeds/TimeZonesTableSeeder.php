<?php

use App\Geo\Country;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TimeZonesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $prepareData = [];

        $file = file_get_contents(database_path('data' . DIRECTORY_SEPARATOR . 'time_zone.txt'));
        $timeZones = explode("\n", $file);

        foreach ($timeZones as $timeZone) {
            $currentTimeZone = explode("\t", $timeZone);

            if (isset($currentTimeZone[0]) && isset($currentTimeZone[1]) && isset($currentTimeZone[2])) {
                $country = Country::where(['iso' => $currentTimeZone[0]])->first();
                if (!is_null($country))
                    array_push($prepareData, [
                        'time_zone' => $currentTimeZone[1],
                        'country_code' => $currentTimeZone[0],
                        'GMT' => $currentTimeZone[2],
                        'created_at' => date('Y-m-d H:i:s', time() - (6 * 60 * 60)),
                        'updated_at' => date('Y-m-d H:i:s', time() - (6 * 60 * 60)),
                    ]);
            }

        }

        DB::table('geo.time_zones')->insert($prepareData);
    }
}
