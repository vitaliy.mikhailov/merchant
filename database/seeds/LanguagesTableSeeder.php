<?php

use App\Geo\Country;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LanguagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $prepareData = [
            [
                'code' => 'en',
                'name' => 'English',
                'emoji_flag' => "\xf0\x9f\x87\xba\xf0\x9f\x87\xb8",
            ],
            [
                'code' => 'ru',
                'name' => 'Русский',
                'emoji_flag' => "\xf0\x9f\x87\xb7\xf0\x9f\x87\xba",
            ],
            [
                'code' => 'kk',
                'name' => 'Қазақ',
                'emoji_flag' => "\xf0\x9f\x87\xb0\xf0\x9f\x87\xbf",
            ],
            [
                'code' => 'uk',
                'name' => 'Український',
                'emoji_flag' => "\xf0\x9f\x87\xba\xf0\x9f\x87\xa6",
            ],
            [
                'code' => 'de',
                'name' => 'Deutsch',
                'emoji_flag' => "\xf0\x9f\x87\xa9\xf0\x9f\x87\xaa",
            ],
            [
                'code' => 'fr',
                'name' => 'Français',
                'emoji_flag' => "\xf0\x9f\x87\xab\xf0\x9f\x87\xb7",
            ],
            [
                'code' => 'es',
                'name' => 'Espanol',
                'emoji_flag' => "\xf0\x9f\x87\xaa\xf0\x9f\x87\xb8",
            ],
            [
                'code' => 'it',
                'name' => 'Italiano',
                'emoji_flag' => "\xf0\x9f\x87\xae\xf0\x9f\x87\xb9",
            ],
            [
                'code' => 'ar',
                'name' => 'عربي',
                'emoji_flag' => "\xf0\x9f\x87\xa6\xf0\x9f\x87\xaa",
            ],
            [
                'code' => 'zh',
                'name' => '中文',
                'emoji_flag' => "\xf0\x9f\x87\xa8\xf0\x9f\x87\xb3",
            ],
        ];

        DB::table('geo.languages')->insert($prepareData);
    }
}
