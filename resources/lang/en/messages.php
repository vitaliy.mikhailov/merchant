<?php

return [

    // Главное меню.
    'lang_menu_msg' => "\xF0\x9F\x8F\x81 Для начала укажите удобный вам язык." . PHP_EOL . "\xF0\x9F\x8F\x81 Please, choice your language",

    // Главное меню.
    'main_menu_msg' => "\xF0\x9F\x93\x8C Main menu.",

    'unknown' => "\xE2\x9A\xA0 You entered an unknown command\n\xF0\x9F\x95\x91 Or this menu hasn't been developed yet.\n\xE2\x84\xB9 You can always return to the main menu with the command /start",

];
