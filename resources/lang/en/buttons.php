<?php

return [

    'base' => [
        'back' => "\xF0\x9F\x94\x99 Back",
        'unknown' => "Для неизвестных кнопок",
    ],

    'main' => [
        // Старт бота.
        'start' => '/start',

        // Главное меню.
        'shops' => "\xF0\x9F\x8F\xAA My shops",
        'disclaimer' => "\xE2\x84\xB9 Disclaimer",
        'pricePolicy' => "\xF0\x9F\x92\xB1 Price policy",
        'tutorial' => "\xF0\x9F\x99\x8B Tutorial",

        // Магазины
        'createShop' => "\xE2\x9E\x95 Create new shop",
    ],

];
