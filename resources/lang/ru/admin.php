<?php

return [

    // Логин
    'login_title' => 'Ленивый инвестор | Вход',
    'project_name' => 'Ленивый инвестор',
    'login_sign_in_msg' => 'Введите свои данные для входа',
    'login_email' => 'Почта',
    'login_password' => 'Пароль',
    'login_sign_in' => 'Войти'

];
