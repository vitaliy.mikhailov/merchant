<?php

return [

    'main' => [
        // Старт бота.
        'start' => '/start',

        // Главное меню.
        'mainMenu' => "\xF0\x9F\x93\x8C Главное меню",
        'shops' => "\xF0\x9F\x8F\xAA Мои магазины",
        'disclaimer' => "\xE2\x84\xB9 Отказ от ответственности",
        'pricePolicy' => "\xF0\x9F\x92\xB1 Ценовая политика",
        'articles' => "\xF0\x9F\x99\x8B Помощь",

        'back' => "\xF0\x9F\x94\x99 Назад",
        'unknown' => "Для неизвестных кнопок",
        'support' => "\xF0\x9F\x99\x8B Попросить помощь",

        'empty' => "",
        'firstPage' => "\xE2\x8F\xAA",
        'nextPage' => "\xE2\x96\xB6",
        'previousPage' => "\xE2\x97\x80",
        'lastPage' => "\xE2\x8F\xA9",
    ],

    'shop' => [
        'createShop' => "\xE2\x9E\x95 Создать новый магазин",
        'enableAllShops' => "\xE2\x9C\x85 Включить все магазины",
        'disableAllShops' => "\xE2\x9D\x8C Выключить все магазины",

        'enableShop' => "\xE2\x9C\x85 Включить магазин",
        'disableShop' => "\xE2\x9D\x8C Выключить магазин",
    ],

    'place' => [
        'places' => "\xF0\x9F\x8C\x8D Города",
        'addPlace' => "\xE2\x9E\x95 Добавить город",
        'enablePlace' => "\xe2\x9c\x85 Включить город",
        'disablePlace' => "\xe2\x98\x91 Отключить город",
    ],

    'category' => [
        'categories' => "\xf0\x9f\x97\x82 Разделы",
        'addCategory' => "\xE2\x9E\x95 Добавить раздел",
        'enableCategory' => "\xe2\x9c\x85 Включить раздел",
        'disableCategory' => "\xe2\x98\x91 Отключить раздел",
        'deleteCategory' => "\xf0\x9f\x97\x91 Удалить раздел",
    ],

    'personal' => [
        'personal' => "\xF0\x9F\x91\xA4 Персонал",
        'addWorker' => "\xE2\x9E\x95 Добавить сотрудника",
        'removeWorker' => "\xE2\x9E\x95 Уволить сотрудника",
    ],

    'permission' => [
        'addPermission' => "\xf0\x9f\x94\x90 Добавить роль",
        'removePermission' => "\xf0\x9f\x94\x92 Снять роль"
    ]

];
