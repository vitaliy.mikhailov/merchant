<?php

return [

    //Вход
    'login_form' => 'Вход',

    // Панель управления
    'dashboard' => 'Панель управления',
    'main_menu' => 'Главная',

    'invest_amount' => 'Сумма инвестиций',
    'invest_returned' => 'Инвестиций возвращено',
    'invest_commission_paid' => 'Выплачено долгов',
    'system_commission_paid' => 'Выплачено комиссий',
    'total_users' => 'Всего пользователей',
    'user_investor' => 'Инвесторов',
    'user_private_investor' => 'Частных инвесторов',
    'bids_at_market' => 'Предложений на рынке',
    'bids_confirmed' => 'Подтвержденных заявок',

    // Пользователи Админки
    'users_menu' => 'Менеджеры',
    'add_user' => 'Создать менеджера',
    'edit_user' => 'Изменить менеджера :userName',

    'user_id' => 'ID',
    'name' => 'Имя',
    'email' => 'Почта/Логин',
    'user_email_placeholder' => 'Будет использоваться для авторизации',
    'password' => 'Пароль',
    'confirm_password' => 'Повторите пароль',
    'roles' => 'Роли',
    'user_role_admin' => 'Администратор системы',
    'user_role_manager' => 'Менеджер',
    'status' => 'Статус',

    'user_status_0' => 'Не активный',
    'user_status_1' => 'Активный',

    'set_data_for_new_user' => 'Укажите данные для нового пользователя',
    'user_added_successful' => 'Пользователь успешно добавлен',
    'set_data_for_edit' => 'Измените старые данные',
    'user_edited_successful' => 'Пользователь успешно обновлен',
    'user_deleted_successful' => 'Пользователь успешно отключен',

    // Инвайты
    'invites_menu' => 'Приглашения',
    'invite' => 'Приглашение',
    'manager' => 'Менеджер',
    'invite_full_name' => 'Ф. И. О.',
    'invite_user_type' => 'Роль',
    'invite_description' => 'Описание',
    'invite_link' => 'Ссылка',

    'set_data_for_new_invite' => 'Укажите данные для нового приглаения',
    'invite_added_successful' => 'Приглашение успешно создано',
    'invite_deleted_successful' => 'Приглашение успешно удалено',

    // Клиенты
    'clients_menu' => 'Клиенты',
    'client_title' => 'Клиент',

    'client_telegram' => 'Телеграм аккаунт',

    'client_user_id' => 'ID',
    'client_telegram_id' => 'Telegram ID',
    'client_username' => 'Telegram username',
    'client_type' => 'Тип',

    'client_personal_data' => 'Данные',

    'client_personal_place' => 'Проживает',
    'client_personal_phone' => 'Телефон',
    'client_personal_email' => 'Почта',

    'client_passport_data' => 'Паспортные данные',

    'client_passport_full_name' => 'Ф. И. О.',
    'client_passport_family' => 'Фамилия',
    'client_passport_first_name' => 'Имя',
    'client_passport_last_name' => 'Отчество',
    'client_passport_birth_date' => 'Дата рождения',
    'client_passport_birth_place' => 'Место рождения',
    'client_passport_serial' => 'Серия',
    'client_passport_number' => 'Номер',
    'client_passport_issue_by' => 'Кем выдан',
    'client_passport_issue_date' => 'Когда выдан',

    'client_commission_liability' => 'Задолженность по комиссии',
    'client_investor_liability' => 'Задолженность по кредитам',

    'investors' => 'Инвесторы',
    'investor' => 'Инвестор',
    'amount_join' => 'Сумма участия',
    'private_investors' => 'Частные инвесторы',
    'private_investor' => 'Частный инвестор',

    // Сделки
    'bids_menu' => 'Сделки',
    'bids_in_work' => 'В работе',
    'bids_exchange' => 'Рынок',
    'bids_closed' => 'Закрытые',
    'bids_personal' => 'Приватные',
    'bids_my' => 'Мои',

    'bid_id' => 'Идентификатор',
    'bid_number' => 'Сделка',
    'bid_author' => 'Автор',
    'bid_place' => 'Город',
    'bid_expire_in' => 'Дата истечения',
    'bid_amount' => 'Сумма',
    'bid_minimal_amount' => 'Минимальная сумма',
    'bid_investor_percent' => 'Процент инвестора',
    'bid_total_month' => 'Срок (мес)',
    'bid_income_type' => 'Тип дохода',
    'bid_income_type_commission' => 'Комиссионный',
    'bid_income_type_fixed' => 'Фиксированный',
    'bid_security' => 'Обеспечение',
    'bid_forward_estimate' => 'Предварительная оценка',
    'bid_payment_type' => 'Оплата',
    'bid_payment_type_cash' => 'Наличные',
    'bid_payment_type_credit_card' => 'На карту',
    'bid_payment_type_settlement_account' => 'На Р/С',
    'bid_payment_type_other' => 'Другое',
    'bid_payment_data' => 'Данные оплаты',
    'bid_accepted_at' => 'Дата заключения',
    'bid_prodded_by' => 'Ответственный',
    'bid_status' => 'Статус',

    'creditor' => 'Кредитор',

    'bid_edited_successful' => 'Заявка успешно обновлена',
    'bid_deleted_successful' => 'Сделка успешно закрыта',

    // Транзакции
    'transactions_menu' => 'Транзакции',
    'add_transaction' => 'Создать транзакцию',

    'transaction_commission' => 'Комиссия',
    'transaction_user' => 'Пользовательские переводы',

    'transaction_id' => 'Идентификатор',

    'transaction_title' => 'Транзакция',
    'transaction_bid_id' => 'Заявка',
    'transaction_sender_id' => 'ID отправителя',
    'transaction_sender' => 'Отправитель',
    'transaction_recipient_id' => 'ID получателя',
    'transaction_recipient' => 'Получатель',
    'transaction_total_amount' => 'Общая сумма',
    'transaction_platform_commission' => 'Комиссия платформы',
    'transaction_loan_liability' => 'Долг по кредиту',
    'transaction_loan_body' => 'Тело кредита',
    'transaction_accepted_by' => 'Обработал',
    'transaction_status' => 'Статус',

    'set_data_for_new_transaction' => 'Создание новой транзакции',
    'transaction_added_successful' => 'Транзакция успешно добавлена',
    'set_data_for_edit_transaction' => 'Измените старые данные',
    'transaction_edited_successful' => 'Транзакция успешно одобрена',
    'transaction_deleted_successful' => 'Транзакция успешно отклонена',
    'transaction_not_found' => 'Транзакция не найдена, либо с ней нельзя проводить подобное действие',
    'transaction_amount_error' => 'Совокупность процента кредита, тела кредита и комиссии платформы не может быть больше общей суммы транзакции',

    // Менеджеры
    'manager_menu' => 'Менеджеры',

    // Уведомления
    'notice_menu' => 'Уведомления',
    'notice_id' => 'Идентификатор',
    'notice_message' => 'Текст', 'notice_viewed_by' => 'Кем просмотрено',
    'notice_status' => 'Статус',
    'notice_status_0' => 'Не прочитано',
    'notice_status_1' => 'Прочитано',

    'new_publish_bid' => 'На рынке опубликованна новая сделка',
    'new_bid_in_work' => 'Одна из сделок перешла в статус подготовки документов',

    'new_commission' => 'Получен запрос на подтверждение оплаты комиссии',

    'new_user' => 'Зарегистрирован новый пользователь',

    'new_manager' => 'Создан новый менеджер',

    // Статусы
    'bid_status_0' => 'Новая',
    'bid_status_1' => 'Опубликована',
    'bid_status_2' => 'Подготовка документов',
    'bid_status_3' => 'Подписание документов',
    'bid_status_4' => 'Заключена',
    'bid_status_5' => 'Не заключена',
    'bid_status_6' => 'Отклонена',
    'bid_status_7' => 'Закрыта',

    'transaction_status_0' => 'Не подтвержден',
    'transaction_status_1' => 'Подтверждена',
    'transaction_status_2' => 'Отклонена',

    'month_1' => 'Январь',
    'month_2' => 'Февраль',
    'month_3' => 'Март',
    'month_4' => 'Апрель',
    'month_5' => 'Май',
    'month_6' => 'Июнь',
    'month_7' => 'Июль',
    'month_8' => 'Август',
    'month_9' => 'Сентябрь',
    'month_10' => 'Октябрь',
    'month_11' => 'Ноябрь',
    'month_12' => 'Декабрь',

    // Кнопки
    'actions' => 'Действия',
    'login_btn' => 'Войти',
    'logout_btn' => 'Выйти',
    'send' => 'Отправить',
    'btn_create' => 'Создать',
    'btn_invite' => 'Пригласить',
    'btn_add' => 'Добавить',
    'btn_save' => 'Сохранить',
    'btn_reset' => 'Отменить',
    'btn_delete' => 'Удалить',
];
